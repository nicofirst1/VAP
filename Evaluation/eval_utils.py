import os

import matplotlib.pyplot as plt
import numpy as np
import skimage.io
from PIL import Image, ImageDraw, ImageFont
from matplotlib.patches import Rectangle
from mrcnn import visualize
from mrcnn.visualize import apply_mask, random_colors
from scipy import ndimage, random
from shapely.geometry import Polygon

##############################
##    USELESs STUFF
##############################
from skimage.measure import find_contours


##############################
##    NON OPTIMIZED
##############################


def find_max_coord(x, y):
    x_max = 0
    x_min = 10000000
    y_max = 0
    y_min = 10000000
    for indice in range(len(x)):
        if x[indice] < x_min:
            x_min = x[indice]
        if y[indice] < y_min:
            y_min = y[indice]
        if x[indice] > x_max:
            x_max = x[indice]
        if y[indice] > y_max:
            y_max = y[indice]
    return [x_max, x_min, y_max, y_min]

def find_max_coord_2(x, y):
    return max(x), min(x), max(y), min(y)


def coordToMatrix_no_transformation(coord, w, h):
    img_size = (w, h)
    poly = Image.new("RGBA", img_size)
    pdraw = ImageDraw.Draw(poly)
    if len(coord) >= 2:
        pdraw.polygon(coord, fill=(0, 0, 255, 100), outline=(0, 0, 255, 255))
    # poly = poly.transpose(Image.FLIP_LEFT_RIGHT)
    # poly = poly.rotate(180)

    # if isPred:
    #    poly = poly.rotate(-90)
    # pix = np.array(poly.getdata()).reshape(w, h)
    return poly


def coordToMatrix(coord, w, h, isPred):
    img_size = (w, h)
    poly = Image.new("RGBA", img_size)
    pdraw = ImageDraw.Draw(poly)
    if len(coord) >= 2:
        pdraw.polygon(coord, fill=(255, 0, 0, 100), outline=(255, 0, 0, 255))
    poly = poly.transpose(Image.FLIP_LEFT_RIGHT)
    poly = poly.rotate(180)

    if isPred:
        poly = poly.rotate(-90)
    # pix = np.array(poly.getdata()).reshape(w, h)
    return poly


def cade_internamente(max, centroide):
    if centroide[0] < max[0] and centroide[0] > max[1]:
        if centroide[1] < max[2] and centroide[1] > max[3]:
            return True
    # print(max,centroide)
    return False


##############################
##    OPTIMIZED
##############################


def true_info_extraction(img, labels, classes, size, idx, draw_image=True):
    """
    Extract information from every label in the current image
    :param labels: (dict) label name to list of coordinates
    :param classes: (dict) classes names to int
    :param size: (tuple) the size of the image
    :returns:
        centroid_true: (list)  of tuples representing coordinates
        aree_true: (list) of floats
        ids_true: (list) of ints, one for every class
        max_coord: (list) of quadruples representing (max_x,min_x,max_y,min_y)
    """
    centroid_true = []
    aree_true = []
    ids_true = []
    max_coord = []
    w, h = size

    im1 = img.copy()
    im1 = im1.convert('RGBA')
    pdraw = ImageDraw.Draw(im1)

    # for every mask in the image
    for name in labels.keys():
        class_id_name = classes[name]

        centroids_tmp = []
        area_tmp = []
        coord_x_tmp = []
        coord_y_tmp = []
        coord_tmp = []
        arr_tmp = np.zeros(shape=(h,w,4))

        for j in range(len(labels[name])):

            coord = []
            coord_x = []
            coord_y = []
            coord_tmp_local = []

            for k in range(len(labels[name][j])):
                x = labels[name][j][k]['x']
                y = labels[name][j][k]['y']
                coord.append(x)
                coord.append(y)
                coord_x.append(x)
                coord_y.append(y)
                coord_x_tmp.append(x)
                coord_y_tmp.append(y)
                coord_tmp_local.append(x)
                coord_tmp_local.append(y)

            # building image and reading array
            immagine = coordToMatrix(coord, w, h, False)
            arr = np.asanyarray(immagine)
            arr_tmp = np.add(arr_tmp, arr)
            centroid = find_centroid(arr)

            centroids_tmp.append(centroid)
            area = compute_area(arr)
            # print(compute_area(np.asanyarray(immagine))==area)
            area_tmp.append(area)

            if ('hand' in name or 'congas' in name):
                ids_true.append(class_id_name)
                max_coord.append(bbox2(arr))

            coord_tmp.append(coord_tmp_local)

        if ('hand' in name or 'congas' in name):
            for index in range(len(centroids_tmp)):
                centroid_true.append(centroids_tmp[index])
                aree_true.append(area_tmp[index])
        else:
            ids_true.append(class_id_name)
            max_coord.append(bbox2(arr_tmp))
            aree_true.append(sum(area_tmp))
            centroid_x = sum([elem[0] for elem in centroids_tmp]) / len(centroids_tmp)
            centroid_y = sum([elem[1] for elem in centroids_tmp]) / len(centroids_tmp)
            centroid_true.append((centroid_x, centroid_y))

        for elem in coord_tmp:
            immagine = coordToMatrix(elem, w, h, False)
            im1.paste(immagine, (0, 0), mask=immagine)
        for index1 in range(len(centroid_true)):
            pdraw.ellipse((centroid_true[index1][0] - 6, centroid_true[index1][1] - 6, centroid_true[index1][0] + 6,
                           centroid_true[index1][1] + 6),
                          fill=(0, 0, 255, 255))

        if draw_image:
            im1.save(f"../Logs/Images/True/true_mask_{idx}_{j}_{name}.png", "PNG")

    return centroid_true, aree_true, ids_true, max_coord


def bbox1(img):
    a = np.where(img != 0)
    bbox = np.max(a[1]), np.min(a[1]), np.max(a[0]), np.min(a[0])
    return bbox

def bbox2(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    return cmax, cmin, rmax, rmin

def calculate_distance(point1, point2):
    print(point1, point2)
    return np.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


def find_centroid(arr):
    arr = np.sum(arr, axis=-1)
    indices = np.where(arr > 0)
    xx = np.sum(indices[1])
    yy = np.sum(indices[0])
    count = indices[0].shape[0]
    return xx / count, yy / count


def pred_info_extraction(im, model, idx, num_class_dict, draw_image=True):
    fig = np.array(im)
    w, h = im.size
    try:
        results = model.detect([fig], verbose=0)
    except Exception as e:
        # print(e)
        return -1

    r = results[0]
    class_ids = list(r['class_ids'])
    masks = r["masks"]
    boxes = r['rois']

    centroid_pred = []
    area_pred = []

    N = boxes.shape[0]
    scores = r['scores']

    im1 = im.copy()
    im1 = im1.convert('RGBA')
    draw = ImageDraw.Draw(im1)

    for i in range(masks.shape[-1]):
        y1 = 0
        x1 = 0
        # Bounding box
        if np.any(boxes[i]):
            y1, x1, y2, x2 = boxes[i]
            draw.rectangle(((x1, y1), (x2, y2)), outline="red")

        # Label
        class_id = class_ids[i]
        score = scores[i] if scores is not None else None
        label = num_class_dict[class_id]
        caption = "{} {:.3f}".format(label, score) if score else label
        font = ImageFont.truetype("arial.ttf", 20)
        draw.text((x1, y1), caption, fill=(0, 255, 0, 255), font=font)

        # Mask
        mask = masks[:, :, i]

        # Mask Polygon
        # Pad to ensure proper polygons for masks that touch image edges.
        padded_mask = np.zeros((mask.shape[0] + 2, mask.shape[1] + 2), dtype=np.uint8)
        padded_mask[1:-1, 1:-1] = mask

        centroid_tmp = []
        area_tmp = []

        contours = find_contours(padded_mask, 0.5)
        for verts in contours:
            # Subtract the padding and flip (y, x) to (x, y)
            verts = np.fliplr(verts) - 1
            verts_tuple = [(elem[0], elem[1]) for elem in verts]

            area_tmp.append(compute_area_4(verts, im.size))
            centroid_tmp.append(find_centroid_4(verts))

            immagine = coordToMatrix_no_transformation(verts_tuple, w, h)
            im1.paste(immagine, (0, 0), mask=immagine)

            # draw.polygon(verts_tuple, fill=(255, 0, 0, 127), outline=(255, 0, 0, 127))

        area_pred.append(sum(area_tmp))
        len_centroids = len(centroid_tmp)
        if len_centroids > 0:
            centroid_x = sum([elem[0] for elem in centroid_tmp]) / len_centroids
            centroid_y = sum([elem[1] for elem in centroid_tmp]) / len_centroids
        else:
            centroid_x = 0
            centroid_y = 0
        centroid_pred.append((centroid_x, centroid_y))

        draw.ellipse((centroid_pred[-1][0] - 6, centroid_pred[-1][1] - 6, centroid_pred[-1][0] + 6,
                      centroid_pred[-1][1] + 6),
                     fill=(0, 0, 255, 255))

        if draw_image:
            im1.save(f"../Logs/Images/Pred/pred_mask_{idx}_{i}_{label}.png", "PNG")

    return [centroid_pred, class_ids, area_pred]


def compute_area_4(verts, size):
    w, h = size
    coords = []
    for elem in verts:
        coords.append(elem[0])
        coords.append(elem[1])
    immagine = coordToMatrix_no_transformation(coords, w, h)
    arr = np.asanyarray(immagine)
    return compute_area(arr)


def find_centroid_4(verts):
    verts = np.asarray(verts)
    sums = np.sum(verts, axis=0)
    xx = sums[0]
    yy = sums[1]
    count = len(verts)

    return xx / count, yy / count


def compute_area(arr):
    """
    Count the white pixels in an image, effectively computing the area
    :param arr: (np.ndarray) a matrix representing a mask
    :return: (float) area
    """
    return arr.any(axis=-1).sum()


def get_right_ones(ids, areas, centroids, max_coord, area_thr=0.5, centroid_thr=0.05, use_bbox=True):
    """
    Calculate the ammount of right prediction given a threshold
    :param ids: (tuple) contains ids_pred and inds_true
    :param areas: (tuple) contains area_pred and area_true
    :param centroids: (tuple) contains centroid_pred and bounding boxes
    :param max_coord:
    :param area_thr: (float) the threshold of correctness
    :param centroid_thr: (float) threshold to apply for centroid distance
    :return: (tuple) of (int,int) which are (succesful pred, total, pred)
    """

    success = 0
    failed_inner = 0

    ids_pred, ids_true = ids
    area_pred, area_true = areas
    centroid_pred, centroid_true = centroids
    already_used_pred_ids = []

    for idx_true in range(len(ids_true)):
        for idx_pred in range(len(ids_pred)):
            # if the class indices are the same
            if ids_pred not in already_used_pred_ids:
                # if the predicted area is in the range of the true one
                if ids_pred[idx_pred] == ids_true[idx_true]:
                    # If the current prediction was already considered for a previous correct prediction
                    if check_area_threshold(area_true[idx_true], area_pred[idx_pred], area_thr):
                        if use_bbox:
                            if cade_internamente(max_coord[idx_true], centroid_pred[idx_pred]):
                                # already_used_pred_ids.append(idx_pred)
                                success += 1
                                break
                            else:
                                failed_inner += 1
                        else:
                            # check if pred centroind is in the image bounding box
                            if check_centroid_threshold2(centroid_true[idx_true], centroid_pred[idx_pred],
                                                         area_true[idx_true], centroid_thr):
                                # already_used_pred_ids.append(idx_pred)
                                success += 1
                                break
                            else:
                                failed_inner += 1

    return success, failed_inner


def check_centroid_threshold2(centroid_pred, centroid_true, area_true, threshold):
    centroid_dist = np.sqrt((centroid_pred[0] - centroid_true[0]) ** 2 + (centroid_pred[1] - centroid_true[1]) ** 2)
    radius_true = np.sqrt(area_true / np.pi)
    radius_true *= threshold

    return centroid_dist < radius_true


##############################
##    OTHER
##############################

def check_area_threshold(area_true, area_pred, threshold):
    under_thr = 1 - threshold
    upper_thr = 1 + threshold

    under_area = area_true * under_thr
    upper_area = area_true * upper_thr

    if under_area < area_pred:
        if upper_area > area_pred:
            return True

    return False


def count_labels(json_parser):
    """
    Count the labels in a whole json file
    :param json_parser: the json object
    :return: (int) number of labels
    """
    total = 0

    for json_elem in json_parser:
        # skip skipped
        if json_elem['Label'] == 'Skip':
            continue

        for name in json_elem['Label'].keys():
            total += len(json_elem['Label'][name])

    return total


def get_classes_dict(json_parser):
    """
    Get a dictionary which maps string (classes name)to int
    :param json_parser: a json tree
    :return: dictionary
    """
    classes = {}
    for json_elem in json_parser:

        if json_elem['Label'] == 'Skip':
            continue

        # add class to class list
        for label in json_elem['Label'].keys():
            if label not in classes.keys():
                classes[label] = len(classes) + 1

    return classes


def print_success_rate(success, total, msg=""):
    """
    Prints  success rate
    :param success: int
    :param total: int
    :return: None
    """
    rate = round(success / total * 100, 2)
    print(f"Over {total}, {success} ({rate}%) {msg}")
