import random
import os
import sys
import random
import colorsys
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
import cv2
import time
import ntpath

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")
DEFAULT_LOGS_DIR = os.path.join(os.getcwd(), "../Logs")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils
from mrcnn import visualize

img_per_gpu = 8
batch_size = img_per_gpu

# Import COCO config
def random_colors(N, bright=True):
    """
    Generate random colors.
    To get visually distinct colors, generate them in HSV space then
    convert to RGB.
    """
    brightness = 1.0 if bright else 0.7
    hsv = [(i / N, 1, brightness) for i in range(N)]
    colors = list(map(lambda c: colorsys.hsv_to_rgb(*c), hsv))
    random.shuffle(colors)
    return colors


def colors(n):
    ret = []
    r = int(random.random() * 256)
    g = int(random.random() * 256)
    b = int(random.random() * 256)
    step = 256 / n
    for i in range(n):
        r += step
        g += step
        b += step
        r = int(r) % 256
        g = int(g) % 256
        b = int(b) % 256
        ret.append((r, g, b))
    random.shuffle(ret)
    return ret


def fill_mask(img, mask, channel, color, h, w):
    for r in range(h):
        for c in range(w):
            if mask[r][c][channel]:
                img[r][c][0] = color[0]
                img[r][c][1] = color[1]
                img[r][c][2] = color[2]
    return img


# txtPath = os.path.dirname(os.path.realpath(__file__))+"/classes.txt" #use this as folder for your classes path
# textfile = open(txtPath, "r")
CLASSES = ["drum_drum",
           "hand",
           "bow",
           "violin",
           "Saxophone",
           "mouth",
           "Accordion",
           "harmonica",
           "congas",
           "guitarra",
           "drums_drum_cops ",
           "piano", ]
CLASSES.sort()

EVALUATION_CLASSES = []
EVALUATION_CLASSES.append('BG')
for CLASS in CLASSES:
    EVALUATION_CLASSES.append(CLASS)

# coco_classes = ['person', 'cat', 'dog', 'handbag', 'suitcase', 'bottle', 'ball', 'cup', 'fork', 'knife', 'spoon',
#                 'apple', 'orange', 'potted plant', 'bed', 'toilet', 'tv', 'laptop', 'mouse', 'cell phone', 'microwave',
#                 'toaster', 'refrigerator', 'clock', 'vase']

coco_classes = [
    'person', 'chair', 'tie', 'bottle', 'clock', 'cell phone',
    'keyboard', 'mouse', 'laptop', 'couch', 'tv', 'broccoli',
    'bus', 'train', 'truck', 'pizza', 'bed', 'remote', 'book', 'bench'
]

for CLASS in coco_classes:
    EVALUATION_CLASSES.append(CLASS)

COLORS = [(231, 119, 39), (45, 189, 109), (115, 3, 179), (31, 175, 95), (52, 196, 116), (217, 105, 25), (38, 182, 102),
          (238, 126, 46), (185, 73, 249), (59, 203, 123), (129, 17, 193), (171, 59, 235), (108, 252, 172),
          (10, 154, 74), (80, 224, 144), (94, 238, 158), (66, 210, 130), (150, 38, 214), (224, 112, 32), (178, 66, 242),
          (17, 161, 81), (157, 45, 221), (210, 98, 18), (87, 231, 151), (122, 10, 186), (24, 168, 88), (143, 31, 207),
          (101, 245, 165), (3, 147, 67), (252, 140, 60), (164, 52, 228), (73, 217, 137), (136, 24, 200), (245, 133, 53),
          (142, 213, 11), (74, 36, 99), (100, 22, 200), (90, 37, 67), (125, 55, 252), (65, 71, 121), (171, 65, 29),
          (9, 45, 192), (158, 59, 147), (112, 118, 82), (0, 23, 13), (32, 42, 72), (221, 248, 243), (180, 220, 2)]

classes_color = colors(len(EVALUATION_CLASSES))


class InferenceConfig(Config):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU

    GPU_COUNT = 1
    IMAGES_PER_GPU = img_per_gpu

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.95
    NUM_CLASSES = len(EVALUATION_CLASSES)
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Give the configuration a recognizable name
    NAME = "vision"


def testVideo(model, pathVideo):
    # random_colors(len(CLASSES))
    print(pathVideo)
    video = cv2.VideoCapture(pathVideo)

    # create dataset folder
    if not os.path.exists('resultsVideo_new/'):
        os.makedirs('resultsVideo_new/')

    if video:
        fps = video.get(cv2.CAP_PROP_FPS)
        print(f'FPS: {fps}')

        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
        print(f'Number of frames in the video: {num_frames}')

        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        print(f'Video resolution --> w: {width}, height: {height}')

        # fourcc = cv2.VideoWriter_fourcc(*'XVID')
        # fourcc = cv2.VideoWriter_fourcc(*'MPEG')
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')

        out = cv2.VideoWriter('resultsVideo_new/' + str(ntpath.basename(pathVideo)), fourcc, fps, (width, height))

        all_frames = []

        for f in range(num_frames):
            ret, frame = video.read()
            if not ret:
                continue
            else:
                all_frames.append(frame)

        num_frames = len(all_frames)


        total_time = 0
        for f in range(0, num_frames, batch_size):
            start = time.time()

            max_index = min(f + batch_size, num_frames)

            if (max_index - f) != batch_size:
                continue

            if f % (batch_size * 10) == 0:
                print(f"Frame: {f}/{num_frames} - {round((f / num_frames) * 100, 2)}%")
                print(f"Seconds per frame : {round(total_time / (f + 1), 2)}")

            r_batch = model.detect(all_frames[f:max_index])
            frame_batch = all_frames[f:max_index]

            for jj in range(batch_size):
                frame = frame_batch[jj]
                r = r_batch[jj]

                overlay = frame.copy()
                for i, id_class in enumerate(r['class_ids']):
                    # Coordinates needed to draw the bounding box around the object
                    x = r['rois'][i][0]
                    y = r['rois'][i][1]
                    w = r['rois'][i][2]
                    h = r['rois'][i][3]

                    # Draw the object's mask on top of the frame
                    fill_mask(overlay, r['masks'], i, COLORS[id_class], height, width)
                    opacity = 0.4
                    cv2.addWeighted(overlay, opacity, frame, 1 - opacity, 0, frame)

                    # Write bounding box and text
                    cv2.rectangle(frame, (y, x), (h, w), COLORS[id_class], 2)
                    cv2.putText(frame, str(EVALUATION_CLASSES[id_class]) + ": " + str(r['scores'][i]), (y, x), 1, 1,
                                COLORS[id_class])

                out.write(frame)
            total_time += time.time() - start

        video.release()
        out.release()


if __name__ == '__main__':

    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect Activities.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'video' or 'image' ")
    parser.add_argument('--source', required=True,
                        metavar="/path/to/source/file",
                        help='Directory of the Source')
    parser.add_argument('--model', required=True,
                        metavar="/path/to/model.h5",
                        help="Path to weights .h5 file")

    parser.add_argument('--category', required=True,
                        metavar="/path/to/model.h5",
                        help="category of videos ('all' for all)")

    args = parser.parse_args()

    config = InferenceConfig()
    config.display()
    # Create model object in inference mode.
    model = modellib.MaskRCNN(mode="inference", model_dir=DEFAULT_LOGS_DIR, config=config)
    # Load weights trained on MS-COCO
    model.load_weights(args.model, by_name=True)

    # Train or evaluate
    if args.command == "image":
        print("image command")
    if args.command == "video":
        file_names = next(os.walk(args.source))[2]
        for f in os.listdir(args.source):
            if f.startswith(args.category) or args.category == 'all':
                pathVideo = os.path.join(args.source, f)
                print(f)
                testVideo(model, pathVideo)
