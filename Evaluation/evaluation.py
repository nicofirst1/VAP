import argparse
import json
import os
import random
import shutil
import traceback

from PIL import Image
from mrcnn import model as modellib
from mrcnn.config import Config

from eval_utils import *

########################
#      CONFIGURATION
#######################


ROOT_DIR = os.getcwd().split("/")[:-1]
ROOT_DIR = "/".join(ROOT_DIR)

# Directory to save logs and trained model
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "Logs")
DATA_DIR = os.path.join(ROOT_DIR, "DATADIR")

weight_index = 85
# head 25
# 4+   70
# all  85

weight_path = f"Weights/mask_rcnn_visiope_00{weight_index}.h5"

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(DATA_DIR, weight_path)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(DATA_DIR, "Visiope_data/Eval/pngImages")


class InferenceConfig(Config):
    """Configuration for training on a local machine with both coco and visiope dataset.
        Derives from the base Config class and overrides values specific
        to the DATADIR.
        """

    # Give the configuration a recognizable name
    NAME = "visiope"

    # We use a GPU with 12GB memory, which can fit two images.
    # 6GB per Image
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1

    # Uncomment to train on 8 GPUs (default is 1)
    # GPU_COUNT = 8

    # Number of classes (including background)
    NUM_CLASSES = 12 + 20 + 1

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 1500

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 50

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Minimum probability value to accept a detected instance
    # ROIs below this threshold are skipped
    DETECTION_MIN_CONFIDENCE = 0.4


config = InferenceConfig()
config.display()

# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=DEFAULT_LOGS_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True, )

IMAGES_LOG_DIR = os.path.join(DEFAULT_LOGS_DIR, "Images/True")
if os.path.exists(IMAGES_LOG_DIR):
    shutil.rmtree(IMAGES_LOG_DIR)
os.makedirs(IMAGES_LOG_DIR)

IMAGES_LOG_DIR_PRED = os.path.join(DEFAULT_LOGS_DIR, "Images/Pred")
if os.path.exists(IMAGES_LOG_DIR_PRED):
    shutil.rmtree(IMAGES_LOG_DIR_PRED)
os.makedirs(IMAGES_LOG_DIR_PRED)

########################
#      STARTING
#######################


json_path = os.path.join(DATA_DIR, "Visiope_data/Eval/merged.json")
json_parser = json.load(open(json_path))
# random.shuffle(json_parser)

# get a dictionary that maps string to id
class_list = ['Accordion', 'hand', 'drum_drum', 'harmonica', 'mouth', 'piano', 'guitarra',
              'violin', 'bow', 'congas', 'Saxophone', 'drums_drum_corps']

class_list = list(sorted(class_list))

classes = {'BG': 0}
num_class_dict = {0: 'BG'}
for elem in class_list:
    classes[elem] = len(classes)
    num_class_dict[len(classes) - 1] = elem

class_list_coco = ['person', 'bus', 'train', 'truck', 'bench', 'tie', 'bottle', 'broccoli',
                   'pizza', 'chair', 'couch', 'bed', 'tv', 'laptop', 'mouse', 'remote',
                   'keyboard', 'cell phone', 'book', 'clock']

for elem in class_list_coco:
    classes[elem] = len(classes)
    num_class_dict[len(classes) - 1] = elem

# if the dict is not provided load it
if not classes:
    classes = get_classes_dict(json_parser)

threshold_area = 0.5
threshold_centroid = 1
use_bbox = True


def main_loop(area_thr, centroid_thr):
    # start iteration
    success = 0
    total = 1
    failed_area = 0
    not_predicted = 0
    idx = 0
    num_images = len(json_parser)
    max_images = num_images
    for json_elem in json_parser:

        # skip skipped
        if json_elem['Label'] == 'Skip':
            continue

        # get the right image path
        img_path = os.path.join(IMAGE_DIR, f"image{idx}.png")
        idx += 1

        if idx > max_images:
            break

        # read the image
        im = Image.open(img_path)
        labels = json_elem["Label"]

        # getting infos from labels
        centroid_true, aree_true, ids_true, max_coord = true_info_extraction(im, labels, classes, im.size, idx, True)

        # doing the same for prediction
        out = pred_info_extraction(im, model, idx, num_class_dict, True)

        if isinstance(out, int):
            # print("Prediction Error")
            continue

        centroid_pred, ids_pred, aree_pred = out

        not_predicted += max(0, len(ids_true) - len(ids_pred))
        total += len(ids_true)

        # there has been an error in the model predic method
        if len(ids_pred) == 0:
            # print("no prediction")
            continue

        # get the number of right answers
        s, fa = get_right_ones((ids_pred, ids_true), (aree_pred, aree_true), (centroid_pred, centroid_true),
                               max_coord, area_thr=area_thr, centroid_thr=centroid_thr, use_bbox=use_bbox)
        success += s
        failed_area += fa

        if idx % 20 == 0:
            print(f"image {idx}/{num_images}")
            wrong = total - success - not_predicted
            predicted = success + wrong

            print()
            print_success_rate(success, total, msg="are correct")
            print_success_rate(wrong, total, msg="wrong predictions")
            print_success_rate(not_predicted, total, msg="not predicted\n")

            print(f'This should sum to {total}: {success + wrong + not_predicted}')
            print_success_rate(failed_area, total, msg="failed from centroid distance\n")

            precision = success / predicted
            recall = success / total

            if precision + recall == 0:
                f1 = 0
            else:
                f1 = 2 * (precision * recall) / (precision + recall)

            print(f'Precision: {round(precision * 100, 2)}%')
            print(f'Recall: {round(recall * 100, 2)}%')
            print(f'F1-measure score: {round(f1 * 100, 2)}%')

        # save_image_infos(im, (centroid_true,centroid_pred),(aree_true,aree_pred),(),(ids_true,ids_pred),idx)

    wrong = total - success - not_predicted
    predicted = success + wrong

    print()
    print_success_rate(success, total, msg="are correct")
    print_success_rate(wrong, total, msg="wrong predictions")
    print_success_rate(not_predicted, total, msg="not predicted\n")

    print(f'This should sum to {total}: {success + wrong + not_predicted}')
    print_success_rate(failed_area, total, msg="failed from centroid distance\n")

    precision = success / predicted
    recall = success / total

    if precision + recall == 0:
        f1 = 0
    else:
        f1 = 2 * (precision * recall) / (precision + recall)

    print(f'Precision: {round(precision * 100, 2)}%')
    print(f'Recall: {round(recall * 100, 2)}%')
    print(f'F1-measure score: {round(f1 * 100, 2)}%')
    print(f"Using model {weight_index}")


main_loop(threshold_area, threshold_centroid)


def save_image_infos(img, centroids, areas, masks, ids, idx):
    circle_radius = 5
    img = img.copy()
    img = img.transpose(Image.FLIP_TOP_BOTTOM)
    draw = ImageDraw.Draw(img)

    centr_true, centr_pred = centroids
    area_true, area_pred = areas
    mask_true, mask_pred = masks
    ids_true, ids_pred = ids

    draw.ellipse((centr_true[0] - circle_radius, centr_true[1] - circle_radius,
                  centr_true[0] + circle_radius, centr_true[1] + circle_radius), fill=(255, 0, 0, 255))
    draw.polygon(mask_true, fill=(0, 255, 0, 10))

    draw.ellipse((centr_true[0] - circle_radius, centr_true[1] - circle_radius,
                  centr_true[0] + circle_radius, centr_true[1] + circle_radius), fill=(0, 255, 0, 255))
    draw.polygon(mask_true, fill=(0, 0, 255, 10))

    img.save(f"{IMAGES_LOG_DIR}/image_{idx}.png", "PNG")
    img = img.transpose(Image.FLIP_TOP_BOTTOM)
    img.show()
    print()
