import os

from pytube import Playlist
from pytube import YouTube

# Root directory of the project
ROOT_DIR = os.getcwd().split("/")[:-2]
ROOT_DIR = "/".join(ROOT_DIR)

# Directory to save logs and trained model
DATA_DIR = os.path.join(ROOT_DIR, "DATADIR")
VIDEOS_DIR = os.path.join(DATA_DIR, "Videos2")

if not os.path.exists(VIDEOS_DIR):
    os.makedirs(VIDEOS_DIR)

# dictionary with links to playlists
playlists = {
    "accordion": ["https://www.youtube.com/watch?v=1VkirI8d-bM&list=PLLSWNIdJv8uspCWe1cfQrR4Vdwt9_d8Ch",
                  "https://www.youtube.com/watch?v=EjhkEqapObA&list=PLLALQuK1NDrgPcppnEfYEpq3ooNVvWdSa",
                  "https://www.youtube.com/watch?v=dJ_SwHb6xMo&list=PLGus4jCeW3_2j61H6ugCx8HjcjMh6-o98",
                  "https://www.youtube.com/watch?v=vz2veabxKrI&list=PLjGI9hR3JLcnBwLF7AYCLqeWyRGrnipf-",
                  "https://www.youtube.com/watch?v=gjO37iWvztw&list=PLjGI9hR3JLcka0wadsf_VZ3Hp-MMymctY"],
    "congas": ["https://www.youtube.com/watch?v=-fhZAARXKFE&list=PL5zAPNXy4wjzf47LbCUd4_6IpaqLGImB_",
               "https://www.youtube.com/watch?v=Jodz2hUIYTg&list=PL5BiJ5aGl5Mq1McGF1cQDKVtoZhsPKvwi",
               "https://www.youtube.com/watch?v=KwdZiQ1oilI&list=PLA9CF7C11DE7D759D"],
    "drum_corps": ["https://www.youtube.com/watch?v=dmNYsWWjaG0&list=PLuFmwZ9ieexDfqY38gkTxYAu1TdJlZCbn",
                   "https://www.youtube.com/watch?v=AfnjYv_RRuk&list=PLYRWKuF0uBgC6-Vnfe15DHNC7F1k-Fub_"],
    "drums": ["https://www.youtube.com/watch?v=zwSqwPSMi7E&list=PLpNTbm3iOlKRlulUYiVeow_mqSy9x6lWc",
              "https://www.youtube.com/watch?v=9esWG6A6g-k&list=PLoEtwljssdlVxmy-A6glTQJLCw9wf87qK"],
    "guitarra": ["https://www.youtube.com/watch?v=4EVT2VNMcpA&list=PL-RYb_OMw7GfG6MS0WBO1v2qvtomUkZci",
                 "https://www.youtube.com/watch?v=15DSV8YkVoo&list=PL_MS9yTfSINV5h-eUAeX_c2uMiuR6j7PY",
                 "https://www.youtube.com/watch?v=_bULnYSWNPE&list=PLiyMO_9U8g1BNzo7ZoXwKg2Pqt5chP6CT"],
    "harmonica": ["https://www.youtube.com/watch?v=-nuhKek8pMg&list=PL24FA74441C6B1801",
                  "https://www.youtube.com/watch?v=_iPKBEUEnJ8&list=PLB756046826AA9336",
                  "https://www.youtube.com/watch?v=vphWgqbF-AM&list=PLm4agQb9N4UjjpLcn6qys0ZvMkBTMMsMe"],
    "piano": ["https://www.youtube.com/watch?v=vphWgqbF-AM&list=PL253192EED47525A8",
              "https://www.youtube.com/watch?v=K7p7AcwAeWY&list=PLuKBHfv8VGdrXAuLRAfkflHrdO3Lj_zXB"],
    "saxophone": ["https://www.youtube.com/watch?v=AjBjnL2ZrJU&list=PLA6F1FC384EA7C8AB",
                  "https://www.youtube.com/watch?v=2mryolmWAP4&list=PLW-k0eqjRgX_QAmrXEsOGSnvMxV7cimsy",
                  "https://www.youtube.com/watch?v=nj5hcQ6_fQ8&list=PL7NHzxii48GWuJqfdUv5HElOQqmiYA6kb",
                  "https://www.youtube.com/watch?v=EBVmeOWpFvc&list=PLLALQuK1NDrhzFktfb8AO9SblaQWVv_L6"],
    "violin": ["https://www.youtube.com/watch?v=g65oWFMSoK0&list=PL1HAe0EHcxh74MTNRLSVEKxWdHYDYw2CG",
               "https://www.youtube.com/watch?v=9xJevTO-ITU&list=PLWtxhVATd9j9ii5IqB9jPzsBh_6URV2hX",
               "https://www.youtube.com/watch?v=88G0O5unNuQ&list=PL6urkeK7KgD6z9rImgxwiqBYZVwq31zlg",
               "https://www.youtube.com/watch?v=DGhPGH2YROA&list=PLFB9B2F0703FAF4ED",
               "https://www.youtube.com/watch?v=ZMChoF-syJ0&list=PLXRcaE1WQdXzLnhEpc4gPehtW0Xld-iIv"],

}


def progress_function(stream, chunk, file_handle, bytes_remaining):
    """
    Print the current progress of the video download
    """
    total = stream.filesize
    print(f"\rvideo downloading... {round((1-bytes_remaining/total)*100,2)}%", end="")




def download_video(link):
    # create youtube object
    yt = YouTube(link, on_progress_callback=progress_function)

    # get stream with filters
    dl_stream = yt.streams.filter(
        adaptive=True, type='video',
    ).order_by('resolution').desc().first()

        # check if video has been already downloaded
    for root, dirs, files in os.walk(save_folder):
        for name in files:
            if name == dl_stream.default_filename:
                return

    # download stream in folder
    dl_stream.download(save_folder)


# maximum number of video per category
max_video_per_category = 100
# chosen categories to download
chosen_categories = ['violin','guitarra','congas']

idx_category = 0

# for every chosen category
for category in chosen_categories:

    idx_link = 0
    # for every link in the vategory
    for link in playlists[category]:

        # stop if the maximum number of videos is reached
        if idx_link > max_video_per_category:
            break

        # create a playlist object
        pl = Playlist(link)

        # create the save path if non existing
        save_folder = f"Playing_{category}/"
        save_folder = os.path.join(VIDEOS_DIR, save_folder)

        if not os.path.exists(save_folder):
            os.makedirs(save_folder)

        # get all the link in the playlists
        pl.populate_video_urls()

        print(f'total videos found:  {len(pl.video_urls)}')
        print('starting download')

        # for every link in the playlist
        list_len = len(pl.video_urls)
        for link in pl.video_urls:

            # break if the videos are more than maximum
            if idx_link > max_video_per_category:
                break

            print(f'download {idx_link}/{max_video_per_category} ({round(idx_link/max_video_per_category*100,2)}%) completed')
            try:
                download_video(link)
            except Exception as e:
                print(f"\n{e}")
            finally:
                idx_link += 1


