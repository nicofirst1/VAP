import _pickle as cPickle
import math
import os
import pickle
import time

import cv2
import numpy as np
from mrcnn import model as modellib
from mrcnn.config import Config

from eval_utils import compute_area, find_centroid

########################
# INFERENCE CONFIGURATION
########################
img_per_gpu = 4
batch_size = img_per_gpu


class InferenceConfig(Config):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU

    GPU_COUNT = 1
    IMAGES_PER_GPU = img_per_gpu

    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.5
    NUM_CLASSES = 33
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Give the configuration a recognizable name
    NAME = "vision"


def process_video(model, pathVideo, activity, max_fps=2):
    """
    Proccess a video by detection objects in frames
    :param model: the model used for detection
    :param pathVideo: the path to the video
    :param activity: the name of the activity
    :param max_fps: max fps
    :return: list of dictionaries, one for each frame
    """
    # open the video
    video = cv2.VideoCapture(pathVideo)

    # initialize the res dict
    res_dict = {'activity': activity, 'frames': [], 'path2video': pathVideo}

    # return if no video is found
    if not video:
        video.release()
        del video
        print(f"Video {pathVideo} not found...skipping")
        return

    fps = video.get(cv2.CAP_PROP_FPS)
    print(f'FPS: {fps}')

    num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    print(f'Number of frames in the video: {num_frames}')

    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print(f'Video resolution --> w: {width}, height: {height}')

    skip = 0

    # if the fps is grather than the maximum determine skip count
    if fps > max_fps:
        perc = 1 - max_fps / fps
        skip = math.ceil(1 / perc)

        print(f"FPS exceed limit, skiping every {skip} frame")

    print(f"Frame count : {num_frames}")

    total_time = 0

    ##################
    # Object detection
    ##################

    frame_count = 0

    # num_frames = min(num_frames, 2000)

    # begin detection on frames
    for f in range(0, num_frames, batch_size * skip):
        start = time.time()

        all_frames = []

        for ii in range(batch_size * skip):
            ret, frame = video.read()
            if not ret:
                continue
            else:
                # skip frame
                if skip and (f + ii) % skip == 0:
                    continue

                all_frames.append(frame)

        if len(all_frames) != batch_size:
            continue

        if f % (batch_size * 10) == 0:
            print(
                f"\rFrame: {f}/{num_frames} ({round((f / num_frames) * 100, 2)}%), "
                f"Frames kept: {frame_count}, {round(total_time / (f + 1), 2)} spf",
                end="")

        res_frames = model.detect(all_frames)

        for frame in res_frames:
            new_frame = []

            frame_count += 1

            # if there are no detected object append empty frame
            if len(frame['rois']) == 0:
                res_dict['frames'].append(new_frame)
                continue

            # for every object detected
            for idx in range(len(frame['rois'])):
                # save static infos
                prov_dict = {'roy': frame['rois'][idx],
                             'class_id': frame['class_ids'][idx],
                             'score': frame['scores'][idx]}

                # get current mask
                current_mask = frame['masks'][:, :, idx]

                # compute are/centroid
                area = compute_area(current_mask)
                centroid = find_centroid(np.expand_dims(current_mask, axis=2))
                # save them
                prov_dict['area'] = area
                prov_dict['centroid'] = centroid
                # append infos in single frame list
                new_frame.append(prov_dict)

            # append frame to list
            res_dict['frames'].append(new_frame)

        del res_frames

        total_time += time.time() - start

    video.release()
    del video

    print(f"\n{round(total_time,2)} seconds for {pathVideo}")

    return res_dict


if __name__ == '__main__':

    # Root directory of the project
    ROOT_DIR = os.getcwd().split("/")[:-2]
    ROOT_DIR = "/".join(ROOT_DIR)

    # Directory to save logs and trained model
    DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "Logs")
    DATA_DIR = os.path.join(ROOT_DIR, "DATADIR")

    weight_index = 85
    weight_path = f"Weights/mask_rcnn_visiope_00{weight_index}.h5"

    # Local path to trained weights file
    COCO_MODEL_PATH = os.path.join(DATA_DIR, weight_path)
    # path to video dir
    VIDEOS_DIR = os.path.join(DATA_DIR, "Videos")

    # configuration class
    config = InferenceConfig()
    config.display()

    # Create model object in inference mode.
    model = modellib.MaskRCNN(mode="inference", model_dir=DEFAULT_LOGS_DIR, config=config)

    # Load weights trained on MS-COCO
    model.load_weights(COCO_MODEL_PATH, by_name=True, )

    # create dataset folder
    save_folder = os.path.join(DATA_DIR, "DetectedJsons/")
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    # get already processed videos
    done_videos = [f for f in os.listdir(save_folder) if os.path.isfile(os.path.join(save_folder, f))]

    # get activity names
    activities = list(os.walk(VIDEOS_DIR))[1:]
    activity_len = len(activities)
    activity_idx = 0

    todo_activities = ['Playing_saxophone', 'Playing_congas', 'Playing_violin']

    # for every activity
    for activity in activities:
        print(f"{activity_idx}/{activity_len} activities")

        activity_name = activity[0].split("/")[-1]
        video_len = len(activity[2])
        video_idx = 0

        if activity_name not in todo_activities:
            continue

        print(f'Doing {activity_name}...')

        # for every video in the activity
        for video in activity[2]:
            print(f"{video_idx}/{video_len} for {activity_name}")

            # get the results path and name
            save_path = f"{save_folder + video.split('.')[0]}.cpkl"
            # if the video have been already processed skip it
            if save_path.split('/')[-1] in done_videos:
                video_idx += 1
                print(f"skipping {video}")
                continue

            # process the video
            res = process_video(model, os.path.join(activity[0], video), activity_name)
            video_idx += 1

            # save the results
            try:
                with open(save_path, 'wb') as outfile:
                    cPickle.dump(res, outfile, protocol=pickle.HIGHEST_PROTOCOL)
            except MemoryError:
                print(f"skipping {video} for memory issues")

            print(f"\nsaved {video} in {save_path}")

        activity_idx += 1
