import _pickle as cPickle
import os
import pickle
import json
import numpy as np

from eval_utils import compute_area, find_centroid

# Root directory of the project
ROOT_DIR = os.getcwd().split("/")[:-2]
ROOT_DIR = "/".join(ROOT_DIR)

DATA_DIR = os.path.join(ROOT_DIR, "DATADIR")

# create dataset folder
save_folder = os.path.join(DATA_DIR, "DetectedJsons/")
if not os.path.exists(save_folder):
    os.makedirs(save_folder)


def convert_pickle():
    # get activity names
    activities = list(os.walk(save_folder))[0]
    activity_len = len(activities[2])
    activity_idx = 0
    # for every video
    for video_name in activities[2]:
        print(f"\r{activity_idx}/{activity_len}",end="")

        # if the video have been already converted skip
        if "cpkl" in video_name:
            activity_idx += 1
            continue

        path2video = activities[0] + video_name

        # try to load the file, fails when there have been a problem dumping
        try:
            with open(path2video, "rb") as file:
                pik = pickle.load(file)
        except Exception:
            os.remove(path2video)
            activity_idx += 1
            continue

        # copy some valeus
        res = {}
        res['activity'] = pik['activity']
        res['path2video'] = pik['path2video']

        frames = []

        # for every frame
        for old_frame in pik['frames']:

            new_frame = []

            # if the frame is empty skip
            if len(old_frame['rois']) == 0:
                frames.append(new_frame)
                continue

            # for every mask in the frame
            for idx in range(len(old_frame['rois'])):
                prov_dict = {}
                prov_dict['roy'] = old_frame['rois'][idx]
                prov_dict['class_id'] = old_frame['class_ids'][idx]
                prov_dict['score'] = old_frame['scores'][idx]
                current_mask = old_frame['masks'][:, :, idx]

                area = compute_area(current_mask)
                centroid = find_centroid(np.expand_dims(current_mask, axis=2))

                prov_dict['area'] = area
                prov_dict['centroid'] = centroid
                new_frame.append(prov_dict)

            frames.append(new_frame)

        res['frames'] = frames

        # remove old format
        os.remove(path2video)
        # change extension
        path2video = path2video.split(".")[0] + ".cpkl"
        # save with cpickle
        with open(path2video, "wb") as file:
            cPickle.dump(res, file, protocol=pickle.HIGHEST_PROTOCOL)

        activity_idx += 1

def pickle2json():
    # get activity names
    activities = list(os.walk(save_folder))[0]
    activity_len = len(activities[2])
    activity_idx = 0
    errors=0
    # for every video
    for video_name in activities[2]:
        print(f"\r{activity_idx}/{activity_len}, {errors}/{activity_len}", end="")


        path2video = activities[0] + video_name
        new_file_path=os.path.join(DATA_DIR, "JDetectedJson/")+video_name.split(".")[0]+".json"


        # try to load the file, fails when there have been a problem dumping
        try:
            with open(path2video, "rb") as pik_file, open(new_file_path, "w") as json_file:
                data = pickle.load(pik_file)

                for frame_idx in range(len(data['frames'])):
                    for idx in  range(len(data['frames'][frame_idx])):
                        try:
                            data['frames'][frame_idx][idx]['roy']=data['frames'][frame_idx][idx]['roy'].tolist()
                            data['frames'][frame_idx][idx]['class_id']=int(data['frames'][frame_idx][idx]['class_id'])
                            data['frames'][frame_idx][idx]['score']=float(data['frames'][frame_idx][idx]['score'])
                            data['frames'][frame_idx][idx]['area']=float(data['frames'][frame_idx][idx]['area'])
                            data['frames'][frame_idx][idx]['centroid']=(int(data['frames'][frame_idx][idx]['centroid'][0]),int(data['frames'][frame_idx][idx]['centroid'][1]))
                        except Exception:
                            del data['frames'][frame_idx]
                            break

                json.dump(data, json_file)


        except Exception as e:
            errors+=1
            continue

        finally:
            activity_idx+=1

if __name__ == '__main__':
    pickle2json()