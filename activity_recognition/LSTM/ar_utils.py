import math
import os
import random
from random import shuffle

import cv2
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix

cm_frames_path = "CM_frames/"


def create_dicts(possible_classes):
    num_class_dict = {}
    class_num_dict = {}

    idx = 0
    for elem in possible_classes:
        num_class_dict[idx] = elem
        class_num_dict[elem] = idx
        idx += 1

    return num_class_dict, class_num_dict


def get_batch(videos_batch, EVALUATION_CLASSES, max_seq_len, sampling_num):
    num_classes = len(EVALUATION_CLASSES)
    batch_input_vectors = []
    batch_seq_len = []

    for video in videos_batch:

        for _ in range(sampling_num):
            video_list = []
            batch_seq_len.append(len(video))

            rnd_ids = random.sample(range(len(video)), min(max_seq_len, len(video)))
            rnd_ids.sort()

            for idx in rnd_ids:
                frame_obj_list = video[idx]
                frame_one_hot = [0.0] * num_classes

                for appeared_class in frame_obj_list:
                    frame_one_hot[EVALUATION_CLASSES.index(appeared_class)] = 1

                video_list.append(frame_one_hot)

            # Padding
            while len(video_list) < max_seq_len:
                video_list.append([0.0] * num_classes)

            video_list = video_list[:max_seq_len]

            batch_input_vectors.append(video_list)

    return batch_input_vectors, batch_seq_len


def split_and_shuffle(x, y, seq_len, percentage):
    x_shuf = []
    y_shuf = []
    seq_len_shuf = []

    index_shuf = list(range(len(x)))
    shuffle(index_shuf)
    for i in index_shuf:
        x_shuf.append(x[i])
        y_shuf.append(y[i])
        seq_len_shuf.append(seq_len[i])

    x = x_shuf
    y = y_shuf
    seq_len = seq_len_shuf

    idx = int(len(x) * percentage)
    x_train = x[:idx]
    x_test = x[idx:]

    y_train = y[:idx]
    y_test = y[idx:]

    seq_len_train = seq_len[:idx]
    seq_len_test = seq_len[idx:]

    x_train = np.asanyarray(x_train, dtype=np.float32)
    x_test = np.asanyarray(x_test, dtype=np.float32)
    y_train = np.asanyarray(y_train, dtype=np.float32)
    y_test = np.asanyarray(y_test, dtype=np.float32)
    seq_len_train = np.asanyarray(seq_len_train, dtype=np.float32)
    seq_len_test = np.asanyarray(seq_len_test, dtype=np.float32)

    return x_train, x_test, y_train, y_test, seq_len_train, seq_len_test


def confusion_matrix_plot(y_true, y_pred, output_classes, base_name, show=False):
    """
    Plot and save confusion matrix into specified dir
    """
    # get paths

    idx = len([elem for elem in os.listdir(cm_frames_path) if base_name in elem])
    file = os.path.join(cm_frames_path, f"{base_name}_{idx}.png")

    # plot confucion matrix
    cm = confusion_matrix(y_true, y_pred, output_classes)

    # create figure and popolate
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(cm)
    plt.title('')
    fig.colorbar(cax)
    ax.set_xticklabels([''] + output_classes, rotation=45)
    ax.set_yticklabels([''] + output_classes)
    plt.xlabel('Predicted')
    plt.ylabel('True')

    # save image
    plt.savefig(file)

    if show:
        plt.show()

    plt.close()


def video_from_cms():
    """
    Create a video from the confusion matrix images
    :return:
    """

    print("Saving cm videos")

    max_sec_gif = 10

    train_cm = [os.path.join(cm_frames_path, elem) for elem in os.listdir(cm_frames_path) if "train" in elem]
    test_cm = [os.path.join(cm_frames_path, elem) for elem in os.listdir(cm_frames_path) if "test" in elem]

    print(f"Number of frames in test, train :{len(train_cm)},{len(test_cm)}")

    train_frames = cv2.imread(train_cm[0])

    h, w, _ = train_frames.shape
    fourcc = cv2.VideoWriter_fourcc(*'XVID')

    fps = 2

    # skip_every_train = math.ceil(len(train_cm) / max_sec_gif * fps)
    # skip_every_test = math.ceil(len(test_cm) / max_sec_gif * fps)
    #
    # del train_cm[skip_every_train - 1::skip_every_train]
    # del test_cm[skip_every_test - 1::skip_every_test]

    train_cm=train_cm[:fps*max_sec_gif]
    test_cm=test_cm[:fps*max_sec_gif]

    print(f"Number of frames in test, train after skipping:{len(train_cm)},{len(test_cm)}")

    train_video = cv2.VideoWriter("train.avi", fourcc, fps, (w, h))
    test_video = cv2.VideoWriter("test.avi", fourcc, fps, (w, h))

    for idx in range(len(train_cm)):
        train_video.write(cv2.imread(train_cm[idx]))
        test_video.write(cv2.imread(test_cm[idx]))

    cv2.destroyAllWindows()
    train_video.release()
    test_video.release()

    print("Cm videos saved")
