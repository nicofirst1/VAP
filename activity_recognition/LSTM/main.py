import shutil
import ujson

import tensorflow as tf
from sklearn.model_selection import train_test_split

from activity_recognition.LSTM.ar_utils import *
from activity_recognition.LSTM.model import ActivityRecognitionModel
# Root directory of the project
from activity_recognition.LSTM.model_keras import train_keras_model
from activity_recognition.LSTM.video_processing_utils import *

ROOT_DIR = os.getcwd().split("/")[:-2]
ROOT_DIR = "/".join(ROOT_DIR)
DATA_DIR = os.path.join(ROOT_DIR, "DATADIR")

save_folder = os.path.join(DATA_DIR, "DetectedJsons")
lstm_data_dir = os.path.join(DATA_DIR, "LSTM_data")

if not os.path.exists(lstm_data_dir):
    os.mkdir(lstm_data_dir)

# get the paths
x_train_path = os.path.join(lstm_data_dir, "x.json")
y_train_path = os.path.join(lstm_data_dir, "y.json")

## INPUT PARAMETERS
json_videos_path = save_folder
# minimum conficende when choosing an object in the frame
minimum_confidence = 0.5
# minimum percentage of frame with object (not empty) to accept the video
minimum_detected_frames = 0.1
# percentage of splitting between train/test
split_percent = 0.2
# maximum number of frames per video
max_seq_len = 77
# remove all coco objects from detection
remove_coco = True
# bring the number of video per category to the minimum
max_videos = True
# control whenever ou want to load the data or process it again
load_data = True
# number of samplings for each video
sampling_num = 3

## MODEL PARAMETERS

num_stacked = 1
hidden_size = 128
learning_rate = 0.001
keep_prob = 0.8
log_path = 'logs'
load_model = False
model_path = 'model'
operation = 'Train'
batch_size = 1024
epochs = 500
reg_constant = 0.07
# get a dictionary that maps string to id
CLASSES = [  # DO NOT CHANGE
    # bg
    "accordion",
    "saxophone",
    'bow',
    'congas',
    'drums',
    'drum_corps',
    'guitarra',
    'hand',
    'harmonica',
    'mouth',
    'piano',
    'violin']

output_classes = [
    "accordion",
    "saxophone",
    'congas',
    'drums',
    'drum_corps',
    'guitarra',
    'harmonica',
    'piano',
    'violin', ]

EVALUATION_CLASSES = []
EVALUATION_CLASSES.append('bg')
for CLASS in CLASSES:
    EVALUATION_CLASSES.append(CLASS)

coco_classes = [
    'person', 'chair', 'tie', 'bottle', 'clock', 'cell phone',
    'keyboard', 'mouse', 'laptop', 'couch', 'tv', 'broccoli',
    'bus', 'train', 'truck', 'pizza', 'bed', 'remote', 'book', 'bench'
]

for CLASS in coco_classes:
    EVALUATION_CLASSES.append(CLASS)


def process_data():
    """
    Process the single pickle files datas and return x/y_train
    """

    print("Processing data...")

    video_structs = get_video_structs(json_videos_path, EVALUATION_CLASSES, minimum_confidence, minimum_detected_frames,
                                      remove_coco, max_videos)

    videos_train = []
    y_train = []

    for video in video_structs:
        videos_train.append(get_video_frames_classes(video))
        video_class = video["activity"]
        y_train.append(output_classes.index(video_class))

    # y_train = np.asarray(y_train)
    print("Data processed")

    return videos_train, y_train


def load():
    print("loading jsons...")
    try:
        # try to load them
        with open(x_train_path, "rb") as x_file, open(y_train_path, 'rb') as y_file:
            videos_train = ujson.load(x_file)
            y_train = ujson.load(y_file)
    except FileNotFoundError:
        # if jsons are not there process the data
        print("Saved jsons not found...processing data")
        videos_train, y_train = process_data()
        dump_json(videos_train, y_train)

    finally:
        print("Jsons loaded")
        return videos_train, y_train


def dump_json(x, y):
    print("Saving")
    # save them at the end
    with open(x_train_path, "w+") as x_file, open(y_train_path, 'w+') as y_file:
        ujson.dump(x, x_file)
        ujson.dump(y, y_file)
    print("Data saved")


def load_data_function():
    """
    Load data either by processing it or by loading it from a json file
    """
    print("Getting video infos...")

    if load_data:

        videos_train, y_train = load()

    else:
        videos_train, y_train = process_data()
        dump_json(videos_train, y_train)

    x_train, x_test, y_train, y_test = train_test_split(videos_train, y_train, test_size=split_percent, random_state=42)

    y_train_aug = []
    for elem in y_train:
        for _ in range(sampling_num):
            y_train_aug.append(elem)

    y_train = y_train_aug

    y_test_aug = []
    for elem in y_test:
        for _ in range(sampling_num):
            y_test_aug.append(elem)

    y_test = y_test_aug

    print(f'Number of total augmented videos train/test: {len(y_train)}, {len(y_test)}')

    x_train, seq_train = get_batch(x_train, EVALUATION_CLASSES, max_seq_len, sampling_num)
    x_test, seq_test = get_batch(x_test, EVALUATION_CLASSES, max_seq_len, sampling_num)

    train = (x_train, y_train, seq_train)
    test = (x_test, y_test, seq_test)

    return train, test


def main_tf():
    train, test = load_data_function()

    x_train, y_train, seq_len_train = train
    x_test, y_test, seq_len_test = test

    # In order to allow GPU memory growth
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    tf.reset_default_graph()
    graph = tf.Graph()

    num_output_classes = len(output_classes)
    vocab_size = len(EVALUATION_CLASSES)

    print(f'Num input classes: {vocab_size} - Num output classes: {num_output_classes}')

    model = ActivityRecognitionModel(graph, vocab_size, max_seq_len, learning_rate,
                                     hidden_size, keep_prob, num_output_classes, num_stacked, reg_constant)

    # empty log dir
    if os.path.exists(log_path):
        shutil.rmtree(log_path)
    os.makedirs(log_path)

    with tf.Session(graph=graph, config=config) as sess:

        with tf.device('/gpu:0'):

            # Open writer and add graph
            writer = tf.summary.FileWriter(log_path, graph=sess.graph)

            if load_model:
                try:
                    model.saver.restore(sess, tf.train.latest_checkpoint(model_path))
                    print('Model successfully loaded.')
                except Exception:
                    print('Unable to load model, initializing variables from scratch.')
                    sess.run(model.init)
            else:
                # Initialize all variables if model is not loaded
                sess.run(model.init)
                print('Model NOT loaded, just doing a normal run.')

            if operation == 'Train':
                print('Creating training data...')

                train = (x_train, y_train, seq_len_train)
                test = (x_test, y_test, seq_len_test)

                model.train(sess, writer, train, test,
                            batch_size, epochs, output_classes)

            elif operation == 'Eval':
                pass

            elif operation == 'Predict':
                pass


def main_keras():
    x, y, _ = load_data()

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.15)

    x_train = np.asanyarray(x_train, dtype=np.float32)
    x_test = np.asanyarray(x_test, dtype=np.float32)
    y_train = np.asanyarray(y_train, dtype=np.float32)
    y_test = np.asanyarray(y_test, dtype=np.float32)

    train = (x_train, y_train)
    test = (x_test, y_test)

    train_keras_model(train, test)


use_keras = False
if __name__ == '__main__':
    if not use_keras:
        main_tf()
    else:
        main_keras()
