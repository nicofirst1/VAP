import os
import pickle


def get_video_frames_classes(video_struct):
    frames = video_struct['frames']

    frames_classes = []

    for object_list in frames:

        frame_obj_list = list()
        for object in object_list:
            name = object['class']
            frame_obj_list.append(name)

        frames_classes.append(frame_obj_list)

    return frames_classes


def get_frames_output_vector(video_struct, class_num_out_dict, max_seq_len):
    """

    :param video_struct: dict with 2 keys: 'activity': name of the activity (name of the instrument only!)
                                           'frames': list of frames, each of which has a list of objects,
                                                     where each object has a list of poly and the object id
    :return:
    """

    # Info = namedtuple('Info', 'area centroid')

    activity_name = video_struct['activity']
    frames = video_struct['frames']

    output_mask = []

    for object_list in frames:

        if len(output_mask) == max_seq_len:
            break

        class_id = 0

        # obj_in_frame = dict()

        for object in object_list:
            name = object['class']

            if name in activity_name:
                class_id = class_num_out_dict[activity_name]

            # area = object['area']
            # centroid = object['centroid']
            #
            # info = Info(area, centroid)
            #
            # if name not in obj_in_frame:
            #     obj_in_frame[name] = [info]
            # else:
            #     obj_in_frame[name].append(info)

        # sax_info_list = None
        # mouth_info_list = None
        # hand_info_list = None
        # person_info_list = None

        # if 'sax' in activity_name:
        #     if 'saxophone' in obj_in_frame:
        #         sax_info_list = obj_in_frame['saxophone']
        #
        #     if 'mouth' in obj_in_frame:
        #         mouth_info_list = obj_in_frame['mouth']
        #
        #     if 'hand' in obj_in_frame:
        #         hand_info_list = obj_in_frame['hand']
        #
        #     if 'person' in obj_in_frame:
        #         person_info_list = obj_in_frame['hand']
        #
        #     if sax_info_list is not None:
        #         for info in sax_info_list:
        #             sax_area = info.area
        #             sax_centroid = info.centroid
        #             sax_radius = math.sqrt(sax_area / math.pi)  # approximation to a circle
        #
        #             sax_hand_check = False
        #             if hand_info_list is not None:
        #                 for info in hand_info_list:
        #                     hand_area = info.area
        #                     hand_centroid = info.centroid
        #                     hand_radius = math.sqrt(hand_area / math.pi)
        #
        #                     distance = abs(hand_centroid.distance(sax_centroid))
        #                     if distance <= (hand_radius + sax_radius):
        #                         sax_hand_check = True
        #                         break
        #
        #             sax_mouth_check = False
        #             if mouth_info_list is not None:
        #                 for info in mouth_info_list:
        #                     mouth_area = info.area
        #                     mouth_centroid = info.centroid
        #                     mouth_radius = math.sqrt(mouth_area) / math.pi
        #
        #                     distance = abs(mouth_centroid.distance(sax_centroid))
        #                     if distance <= (mouth_radius + sax_radius):
        #                         sax_mouth_check = True
        #                         break
        #
        #             sax_person_check = False
        #             if person_info_list is not None:
        #                 for info in person_info_list:
        #                     person_area = info.area
        #                     person_centroid = info.centroid
        #                     person_radius = math.sqrt(person_area) / math.pi
        #
        #                     distance = abs(person_centroid.distance(sax_centroid))
        #                     if distance <= (person_radius + sax_radius):
        #                         sax_person_check = True
        #                         break
        #
        #             if sax_mouth_check or sax_hand_check or sax_person_check:
        #                 class_id = class_num_dict[activity_name]
        #                 break

        output_mask.append(class_id)

    while len(output_mask) < max_seq_len:
        output_mask.append(0)

    window_size = 10
    num_frames = len(output_mask)
    eps = 0.0001
    threshold = 1

    num_replaced_labels = 0

    for i in range(num_frames):
        start_left = max(0, i - window_size)
        end_left = max(0, i)

        start_right = min(num_frames, (i + 1))
        end_right = min(num_frames, (i + 1) + window_size)

        left_chunk = output_mask[start_left:end_left]
        right_chunk = output_mask[start_right:end_right]

        left_ratio = left_chunk.count(class_num_out_dict[activity_name]) / (len(left_chunk) + eps)
        right_ratio = right_chunk.count(class_num_out_dict[activity_name]) / (len(right_chunk) + eps)

        if left_ratio > threshold and right_ratio > threshold:
            output_mask[i] = class_num_out_dict[activity_name]
            num_replaced_labels += 1

    print(f'Number of replaced labels due to "interpolation": {num_replaced_labels}/{len(frames)}')

    return output_mask, activity_name


def pickle_format(video, classes, min_score, coco_removal):
    """
    Read a pickle video and extract info
    :param video: the video as a list of frames
    :param min_score: minimum confidence to keep prediction
    :returns:
        res_dict: a dictionary which hold all the frames infos
        dict: a dictionary which hold other infos
    """

    discarded = 0
    found = 0
    total = len(video['frames'])

    video_pickle = []

    for frame in video['frames']:

        if not frame:
            continue

        sub_list = []
        for elem in frame:

            if elem['score'] < min_score:
                discarded += 1
                continue

            if elem['class_id'] == 13:
                discarded += 1
                continue

            if coco_removal:

                if elem['class_id'] > 12:
                    discarded += 1
                    continue

            sub_list.append({
                'roy': elem['roy'],
                'class': classes[elem['class_id']],
                'area': elem['area'],
                'centroid': elem['centroid']
            })

        if len(sub_list) > 0:
            video_pickle.append(sub_list)
            found += 1

    return video_pickle, {'total': total, 'found': found, 'discarded': discarded}


def get_video_structs(path, classes, min_score, minimum_detected_frames, remove_coco, max_videos):
    """
    Read the pickle frames from file and use convert them into anther format
    :param path: the path to the video directory
    :param classes: the classes list (ordered)
    :param min_score: minimum value for detection confidence (for a mask)
    :param minimum_detected_frames: minimum percentage of frames detected in a video
    :return:
    """

    # ex=toy_example()

    videos = list(os.walk(path))[0]
    videos = [videos[0] + "/" + elem for elem in videos[2]]
    videos_res = []

    discarded = 0

    activity_count = {}

    for video in videos:
        # read the pickle
        with open(video, "rb") as file:
            video_pickle = pickle.load(file)

        frames, infos = pickle_format(video_pickle, classes, min_score, remove_coco)

        if infos['total'] == 0:
            infos['total'] = 1

        # if there are less detected frames than the threshold, discrd video
        if infos['found'] / infos['total'] < minimum_detected_frames:
            discarded += 1
            continue

        activity_name = video_pickle['activity'].split("_", 1)[1].lower()

        if activity_name not in activity_count:
            activity_count[activity_name] = 0

        activity_count[activity_name] += 1

    print(activity_count)

    activity_count_min = None
    for elem in activity_count:
        count = activity_count[elem]
        if activity_count_min is None:
            activity_count_min = count
        elif count < activity_count_min:
            activity_count_min = count

    print(f'Min num of videos in one class: {activity_count_min}')

    activity_count = {}

    # for every video in the dir
    for video in videos:

        # read the pickle
        with open(video, "rb") as file:
            video_pickle = pickle.load(file)

        # process the file and get infos
        frames, infos = pickle_format(video_pickle, classes, min_score, remove_coco)

        if infos['total'] == 0:
            infos['total'] = 1

        # print(
        #     f"Out of {infos['total']} frames, {infos['found']} were found ({round(infos['found']/infos['total']*100,2)}%), "
        #     f"{infos['discarded']} were discarded ({round(infos['discarded']/infos['total']*100,2)}%)")

        # if there are less detected frames than the threshold, discrd video
        if infos['found'] / infos['total'] < minimum_detected_frames:
            discarded += 1
            continue

        activity_name = video_pickle['activity'].split("_", 1)[1].lower()

        if activity_name not in activity_count:
            activity_count[activity_name] = 0

        if max_videos and activity_count[activity_name] >= activity_count_min:
            continue

        # todo: filer out videos with less than toatl_frames*thr for activity name
        activity_count[activity_name] += 1

        frames = [elem for elem in frames if len(elem) > 0]
        video_pickle['activity'] = activity_name
        video_pickle['frames'] = frames
        # Counter([elem['class'] for sub in frames for elem in sub])
        # append video to results
        videos_res.append(video_pickle)

    print(f'Total number of videos: {len(videos_res)}')
    print(f"Discarded {discarded}/{len(videos)} videos")
    print(activity_count)
    return videos_res


def extract_video_infos(videos):
    possible_classes = set()

    for video in videos:
        for frame_obj_set in video:
            for obj in frame_obj_set:
                possible_classes.add(obj)

    possible_classes = list(possible_classes)
    possible_classes.sort()

    return possible_classes
