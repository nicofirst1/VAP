import shutil

import tensorflow as tf
from sklearn.metrics import precision_recall_fscore_support
from tensorflow.contrib import rnn

from activity_recognition.LSTM.ar_utils import *


def tile_repeat(n, repTime):
    '''
    create something like 111..122..2333..33 ..... n..nn
    one particular number appears repTime consecutively.
    This is for flattening the indices.
    '''
    print(n, repTime)
    idx = tf.range(n)
    idx = tf.reshape(idx, [-1, 1])  # Convert to a n x 1 matrix.
    idx = tf.tile(idx, [1, repTime])  # Create multiple columns, each column has one number repeats repTime
    y = tf.reshape(idx, [-1])
    return y


def extract_axis_1(data, ind):
    """
    Get specified elements along the first axis of tensor.
    :param data: Tensorflow tensor that will be subsetted.
    :param ind: Indices to take (one for each element along axis 0 of data).
    :return: Subsetted tensor.
    """

    batch_range = tf.range(tf.shape(data)[0])
    indices = tf.stack([batch_range, ind], axis=1)
    res = tf.gather_nd(data, indices)

    return res


def first_nonzero(arr, axis, invalid_val=-1):
    mask = arr != 0
    return np.where(mask.any(axis=axis), mask.argmax(axis=axis), invalid_val)


class ActivityRecognitionModel:

    def __init__(self, graph, vocab_size, max_seq_len, learning_rate, hidden_size,
                 keep_prob, num_output_classes, num_stacked, reg_constant):

        self.num_output_classes = num_output_classes
        print(f"max seq len : {max_seq_len}")

        with tf.device('/gpu:0'):
            with graph.as_default():
                with tf.name_scope('seq_len'):
                    self.seq_len = tf.placeholder(tf.int32, shape=[None])

                with tf.name_scope('padding_mask'):
                    self.padding_mask = tf.cast(tf.sequence_mask(self.seq_len, maxlen=max_seq_len), dtype=tf.float32)

                with tf.name_scope('batch_size'):
                    self.batch_size = tf.shape(self.seq_len)[0]

                with tf.name_scope('input'):
                    self.x = tf.placeholder(tf.float32, shape=[None, max_seq_len, vocab_size],
                                            name="input_words")

                with tf.name_scope('true_classes'):
                    self.y = tf.placeholder(tf.int32, shape=[None])
                    # [batch_size, max_seq_len]

                with tf.name_scope('bilstm'):
                    self.bilstm_output = self.bilstm(num_stacked, hidden_size, keep_prob, self.x, self.seq_len)
                    # [batch_size, max_seq_len, 2 * hidden_size]

                    bilstm_output_cropped = extract_axis_1(self.bilstm_output, self.seq_len)
                    # [batch_size, 2 * hidden_size]

                    self.histograms_summaries = [tf.summary.histogram('last_hidden_output', bilstm_output_cropped)]

                    c = self.attention(self.bilstm_output, self.padding_mask, max_seq_len)

                    bilstm_output_cropped = tf.concat([bilstm_output_cropped, c], -1)

                with tf.name_scope('softmax_weights'):
                    W = tf.get_variable(shape=[4 * hidden_size, num_output_classes], dtype=tf.float32,
                                        initializer=tf.truncated_normal_initializer(stddev=0.05),
                                        name='W')  # [(2 or 4)*hidden_size, nums]

                    b = tf.get_variable(shape=[num_output_classes], dtype=tf.float32,
                                        initializer=tf.constant_initializer(value=0.0),
                                        name='b')  # [nums]

                self.histograms_summaries.append(tf.summary.histogram('W_output', W))
                self.histograms_summaries.append(tf.summary.histogram('b_output', b))

                with tf.name_scope('softmax_output'):
                    self.logits = tf.nn.xw_plus_b(bilstm_output_cropped, W, b)
                    # [batch * max_seq_len, num_classes]

                    # Obtain an actual probability distribution over the predicted meanings with a softmax
                    self.prob_distribution = tf.nn.softmax(self.logits)
                    # [batch_size, max_seq_len, num_classes]

                with tf.name_scope('loss'):
                    # Sparse softmax cross entropy applies softmax internally
                    losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits,
                                                                            labels=self.y)
                    # [batch_size, max_seq_len]

                    # Computes a loss averaging the average loss of each sentence
                    self.loss = tf.reduce_mean(losses, name="loss_train")  # scalar

                    reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                    self.loss = self.loss + reg_constant * sum(reg_losses)

                # Add to summary
                self.loss_summary = tf.summary.scalar('loss_train', self.loss)

                with tf.name_scope('prediction'):
                    #self.pred_class_ids = tf.argmax(self.prob_distribution, axis=-1)
                    self.pred_class_ids = tf.nn.top_k(self.prob_distribution, k=3)
                    # [batch_size, max_seq_len]

                    self.pred_prob = tf.reduce_max(self.prob_distribution, axis=-1)
                    # [batch_size, max_seq_len]

                with tf.name_scope('optimizer'):
                    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
                    grads = optimizer.compute_gradients(self.loss)
                    clipped_grads = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in grads]
                    self.train_op = optimizer.apply_gradients(clipped_grads)
                    self.train_op = optimizer.minimize(self.loss)

                with tf.name_scope('tensorboard'):
                    self.precision_train = tf.placeholder(tf.float32, shape=(), name="precision_train")
                    self.recall_train = tf.placeholder(tf.float32, shape=(), name="recall_train")
                    self.f1_train = tf.placeholder(tf.float32, shape=(), name="f1_train")

                    self.precision_test = tf.placeholder(tf.float32, shape=(), name="precision_test")
                    self.recall_test = tf.placeholder(tf.float32, shape=(), name="recall_test")
                    self.f1_test = tf.placeholder(tf.float32, shape=(), name="f1_test")

                    self.metrics_summaries = [
                        tf.summary.scalar('train_precision', self.precision_train),
                        tf.summary.scalar('train_recall', self.recall_train),
                        tf.summary.scalar('train_f1', self.f1_train),

                        tf.summary.scalar('test_precision', self.precision_test),
                        tf.summary.scalar('test_recall', self.recall_test),
                        tf.summary.scalar('test_f1', self.f1_test)
                    ]

                    # Merge all summaries
                    self.merged_metrics_summaries = tf.summary.merge(self.metrics_summaries)

                    self.merged_hists_summaries = tf.summary.merge(self.histograms_summaries)

                self.merged_metrics_summaries = tf.summary.merge(self.metrics_summaries)

                # Create saver
                self.saver = tf.train.Saver()

                # Initialize all variables
                self.init = tf.global_variables_initializer()

    def train(self, sess, writer, train, test,
              batch_size, epochs, output_classes):

        x_train, y_train, seq_len_train = train
        x_test, y_test, seq_len_test = test

        batch_loss = None

        path2dir = "CM_frames/"
        if os.path.exists(path2dir):
            shutil.rmtree(path2dir)
        os.makedirs(path2dir)

        iters_train = int(math.ceil(len(x_train) / batch_size))
        print('Batches (iterations) in one training epoch: ', iters_train)

        iters_test = int(math.ceil(len(x_test) / batch_size))
        print('Batches (iterations) in one test epoch: ', iters_test)

        max_f1 = 0

        for epoch in range(epochs):
            print(f"epoch {epoch}/{epochs}")


            y_true_train = []
            y_pred_train = []
            y_true_test = []
            y_pred_test = []

            for it in range(iters_train):

                start = it * batch_size
                end = min((it + 1) * batch_size, len(x_train))

                batch_input_vectors = x_train[start:end]
                output_vectors_batch = y_train[start:end]
                batch_seq_len = seq_len_train[start:end]

                feed_dict = {self.x: batch_input_vectors,
                             self.y: output_vectors_batch,
                             self.seq_len: batch_seq_len,
                             }

                _, batch_pred_class_ids, batch_loss, loss_sums, hists = \
                    sess.run([self.train_op,
                              self.pred_class_ids,
                              self.loss,
                              self.loss_summary,
                              self.merged_hists_summaries
                              ],
                             feed_dict=feed_dict)

                writer.add_summary(loss_sums, global_step=(epoch * iters_train + it))
                writer.add_summary(hists, global_step=(epoch * iters_train + it))

                for idx in range(len(batch_pred_class_ids)):
                    pred = batch_pred_class_ids[idx]

                    y_true_train.append(output_classes[int(y_train[idx])])
                    y_pred_train.append(output_classes[int(pred)])

            for it in range(iters_test):
                start = it * batch_size
                end = min((it + 1) * batch_size, len(x_test))

                batch_input_vectors = x_test[start:end]
                output_vectors_batch = y_test[start:end]
                batch_seq_len = seq_len_test[start:end]

                feed_dict = {self.x: batch_input_vectors,
                             self.y: output_vectors_batch,
                             self.seq_len: batch_seq_len,
                             }

                batch_pred_class_ids = sess.run([self.pred_class_ids, ], feed_dict=feed_dict)[0]

                for idx in range(len(batch_pred_class_ids)):
                    pred = batch_pred_class_ids[idx]

                    y_true_test.append(output_classes[int(y_test[idx])])
                    y_pred_test.append(output_classes[int(pred)])

            confusion_matrix_plot(y_true_train, y_pred_train, output_classes, "train", show=False)
            confusion_matrix_plot(y_true_test, y_pred_test, output_classes, "test", show=False)

            precision_train, recall_train, f1_train, _ = precision_recall_fscore_support(y_true_train, y_pred_train,
                                                                                         labels=output_classes,
                                                                                         average='weighted')
            precision_test, recall_test, f1_test, _ = precision_recall_fscore_support(y_true_test, y_pred_test,
                                                                                      labels=output_classes,
                                                                                      average='weighted')

            if f1_test > max_f1:
                max_f1 = f1_test
                self.save_model(sess, 'model')

            feed_dict = {self.precision_train: precision_train,
                         self.recall_train: recall_train,
                         self.f1_train: f1_train,
                         self.precision_test: precision_test,
                         self.recall_test: recall_test,
                         self.f1_test: f1_test
                         }

            metrics_sums = sess.run(self.merged_metrics_summaries,
                                    feed_dict=feed_dict)

            writer.add_summary(metrics_sums, global_step=epoch)




            print(f'Loss value: {round(batch_loss,3)}')
            print(
                f"Precision train, {round(precision_train*100,2)}%, Recall: {round(recall_train*100,2)}%, F1: {round(f1_train*100,2)}%")
            print(
                f"Precision test, {round(precision_test*100,2)}%, Recall: {round(recall_test*100,2)}%, F1: {round(f1_test*100,2)}%")

            print("\r\r\r\r",end="")

        video_from_cms()

    def bilstm(self, num_stacked, hidden_size, keep_prob, inputs, seq_len):

        # Forward and backward cells
        cells_fw = []
        cells_bw = []

        for i in range(num_stacked):

            cell_fw = rnn.LSTMCell(hidden_size, use_peepholes=True)
            if abs(1 - keep_prob) > 0.00001:
                cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw,
                                                        input_keep_prob=keep_prob,
                                                        output_keep_prob=keep_prob,
                                                        state_keep_prob=keep_prob)
            cells_fw.append(cell_fw)

            cell_bw = rnn.LSTMCell(hidden_size, use_peepholes=True)
            if abs(1 - keep_prob) > 0.00001:
                cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw,
                                                        input_keep_prob=keep_prob,
                                                        output_keep_prob=keep_prob,
                                                        state_keep_prob=keep_prob)
            cells_bw.append(cell_bw)

        output, _, _ = rnn.stack_bidirectional_dynamic_rnn(cells_fw=cells_fw,
                                                           cells_bw=cells_bw,
                                                           inputs=inputs,
                                                           sequence_length=seq_len,
                                                           dtype=tf.float32)

        return tf.concat(output, axis=-1)

    def attention(self, H, mask, max_seq_len):
        """
        Returns attention vector for the sentences.

        :param H: output of the bi-lstm of shape [batch_size, max_seq_len, hidden_size]
        :param mask: bit mask for the attention
        :return: single attention
        """

        hidden_size = H.shape[2].value

        # Trainable parameters
        omega = tf.get_variable(shape=[hidden_size, 1], dtype=tf.float32,
                                initializer=tf.contrib.layers.xavier_initializer(),
                                name='omega')

        b = tf.get_variable(shape=[max_seq_len], dtype=tf.float32,
                            initializer=tf.contrib.layers.xavier_initializer(),
                            name='b_omega')

        self.histograms_summaries.append(tf.summary.histogram('attention_omega', omega))
        self.histograms_summaries.append(tf.summary.histogram('attention_b', b))

        omega_H = tf.tensordot(H, omega, axes=1)  # [batch_size, max_seq_len, 1]
        omega_H = tf.squeeze(omega_H, -1)  # [batch_size, max_seq_len]

        omega_H_plus_b = omega_H + b  # [batch_size, max_seq_len]

        u = tf.tanh(omega_H_plus_b)  # [batch_size, max_seq_len]

        # Apply softmax
        a = tf.exp(u)
        a = a * mask
        a /= tf.reduce_sum(a, axis=1, keepdims=True) + 0.0000001
        # [batch_size, max_seq_len]

        w_inputs = H * tf.expand_dims(a, -1)
        # [batch_size, max_seq_len, hidden_size]

        c = tf.reduce_sum(w_inputs, axis=1)
        # [batch_size, hidden_size]

        return c

    def save_model(self, sess, path):
        """
        Saves the model.

        :param sess: current session
        :param path: path where to save the model
        :return: nothing
        """
        if not os.path.exists(path):
            os.makedirs(path)

        self.saver.save(sess, os.path.join(path, 'model'))
        # print('\nModel saved.')
