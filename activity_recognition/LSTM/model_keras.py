'''Trains an LSTM model on the IMDB sentiment classification task.
The dataset is actually too small for LSTM to be of any advantage
compared to simpler, much faster methods such as TF-IDF + LogReg.
# Notes
- RNNs are tricky. Choice of batch size is important,
choice of loss and optimizer is critical, etc.
Some configurations won't converge.
- LSTM loss decrease patterns during training can be quite different
from what you see with CNNs/MLPs/etc.
'''
from __future__ import print_function

from keras.optimizers import Adam
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding, Dropout, Activation
from keras.layers import LSTM
from keras.datasets import imdb

max_features = 33
batch_size = 32
max_seq_len = 3000

def train_keras_model(train, test):
    x_train, y_train = train
    x_val, y_val = test

    x_train = x_train[:-(len(x_train) % batch_size)]
    x_val = x_val[:-(len(x_val) % batch_size)]
    y_train = y_train[:-(len(y_train) % batch_size)]
    y_val = y_val[:-(len(y_val) % batch_size)]


    print('x_train shape:', x_train.shape)
    print('x_val shape:', x_val.shape)

    print('Build model...')
    model = Sequential()
    layers = [33, 50, 100, 1]
    #model.add(Embedding(max_features, 128))
    model.add(LSTM(
        input_dim=layers[0],
        output_dim=layers[1],
        return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(
        layers[2],
        return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(
        output_dim=layers[3]))
    model.add(Activation("linear"))

    optimizer = Adam(lr=0.0001, clipvalue=1.0)
    # try using different optimizers and different optimizer configs
    model.compile(loss='mse',
                  optimizer="rmsprop",
                  metrics=['accuracy'])

    print('Train...')
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=50,
              validation_data=(x_val, y_val))
    score, acc = model.evaluate(x_val, y_val,
                                batch_size=batch_size)
    print('Test score:', score)
    print('Test accuracy:', acc)
