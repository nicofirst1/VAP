﻿
# Intro 

This is a repo implementing the [coco Mask_RCNN](https://github.com/matterport/Mask_RCNN) by *Matterport* using labels from [LabelBox](https://www.labelbox.io/).
Our job was to work with *musical instruments*, meaning that we have some pretty specific categories. 
Since LabelBox does not provide a splitting operation between different objects with the same mask in the same images, we split them in the *VisiopeClass* through the *load_mask* method. This splitting is done specifically for the *hand* and the *congas* classes, but can be extended to others by simply changing the relative *if condition*

# Setup

The setup is straightforward, you just need to follow [these instructions](https://github.com/matterport/Mask_RCNN)

In order to install correctly OpenCV 2 use the following command:

pip install opencv-python


# Repo Structure

## Utils

The *utils* directory contains the dataset generation part. The order of each file should be the following:

- **image_crawler** : a reasonable instrument for downloading the images to work on; simply specify the strings to look for on the web and the maximum number of images to save for each of them to start the process.
- **json_utils/merge_jsons** : a script to merge multiple JSONs for different objects into a single one, respecting the structure used by LabelBox. To use it, simply place all of the JSONs in the *DATADIR/Jsons* directory and launch this script.
- **json_utils/split_merged_json** : this script will split a JSON file into two with a 4:1 ratio, both named "merged.json", into the *DATADIR/Train* and *DATADIR/Eval* folders. As the name implies, the two sets of images and masks will be used for training the model and evaluating its performance.
- **JSONextractor** : it contains the function to extract the images/masks from the JSON files (train/eval) and correctly name each of the formers for later usage.
- **tester** : use this script to run the JSON extraction process; simply set the path to the working directory containing the JSON file and its name to extract the images and the corresponding masks.

## DATADIR

This directory must strictly contain the data in the following structure:

- [Weights](https://github.com/matterport/Mask_RCNN/releases) from the coco dataset should be placed at the same level  of the *Train/Eval* directories. The filename can be changed in the *Visiope/main* file.
- The *Jsons* directory should contain the various JSONs of the objects, directly downloaded form the [LabelBox](https://www.LabelBox.io/) website (for more infos about the JSON format see Json section).
- Both the *Train/Eval* dirs are symmetrical in terms of structure and should not be modified, since the dataset generation is automatically handled; each directory contains a *merged.json* file and two subfolders called *bmpImages* and *pngImages*, containing the images and all of the associated masks in the two formats.

## Visiope

This is the directory in which all the dataset processing is performed. For modularity and simplicity it has been divided into five files:

- **original** : the original implementation from the [coco sample](https://github.com/matterport/Mask_RCNN/tree/master/samples/coco).
- **utils** : DEPRECATED.
- **VisiopeClass** : The file in which the *mrcnn.config.Config* and *mrcnn.utils.Dataset* classes are overwritten to work with our dataset.
- **main** : The only file to be launched with `python main.py` for the actual execution.  You can specify the various parameters such as the *MODE* (train, eval detect) inside the file itself.

## Visiope_FloydHub
 This folder has the same structure of the previous *Visiope* one, but it is optimized for [FloydHub](https://www.floydhub.com/jobs)
 
# Instance segmentation

## Classes

The classes taken in consideration for the instance segmentation are the following:
- drums (drum_drum)
- hand
- bow
- violin
- saxophone
- mouth
- accordion
- harmonica
- congas
- guitarra
- drum cops (drum_drum_corps)
- piano
- background

The total number of labels is 13.
 
# Before running
Before running the main class you should customize the parameters which control the flow of the program. 
There are two classes where you can edit these parameters:

## main.py

In here you can control the following parameters:

- The various paths which can be configured depending on your dataset.
- **model_weights** : the coco weights you want to use for your model (either v1, v2, or balloon).
- **MODE** : it specifies the behavior of the program; it can be *train*, *eval* or *detect* (this last one perform the instance segmentation on *detection_step* random images).
- **ep1/2/3**: the number of epochs for the three phase training.

## VisiopeClass

You need to modify everything which is in the *CocoConfig* class.

# Problems 

## Issue: Dimensions
>ValueError: Dimension 1 in both shapes must be equal, but are 0 and 324. Shapes are [1024,0] and [1024,324]. for 'Assign_682' (op: 'Assign') with input shapes: [1024,0], [1024,324].

In *main_visiope.py/model.load_weights(model_path, by_name=True)*

**SOLVED**

Solved by setting 
> model.load_weights(model_path, by_name=True, exclude=[
    "mrcnn_class_logits", "mrcnn_bbox_fc",
    "mrcnn_bbox", "mrcnn_mask"])
 
## Issue: Overfitting

Overfitting in both *val_loss* (after the 60th epochs) and *val_mrcnn_mask_loss* (loss grows from the start)

### Data augmentation
Since we have a rather small dataset (circa 900 images), data augmentation is fundamental to prevent overfitting. 
We replaced the original augmentation class 

`augmentation = imgaug.augmenters.Fliplr(0.5)`

with a more advanced one, which performs various types of augmentation
>augmentation = iaa.SomeOf((0, 2), [
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
        iaa.OneOf([iaa.Affine(rotate=90),
                   iaa.Affine(rotate=180),
                   iaa.Affine(rotate=270)]),
        iaa.Multiply((0.8, 1.5)),
        iaa.GaussianBlur(sigma=(0.0, 5.0))
    ])
    
### Early Stopping

Since the values of the validation loss are avaiable in the keras function we slightly modified the *Mask_RCNN* 
[class](https://gitlab.com/nicofirst1/Mask_RCNN_custom.git) to pass a list of *keras.callbacks* to the train function of the model.

We tracked two loss values:

- **val_loss** : the overall loss value, for a complete view of the system.
- **val_mrcnn_mask_loss** : the mask related loss value since it is the only one which grows but never decreases.

### Different weights

We tried using different coco weights versions, we switched from *v1* to *v2*, the mask accuracy increased substantially.

### BackBone

We tried changing the model backbone from *resnet101* to *resnet50*. As result the loss went to infinity in just two steps

## Slow Training

Due to the fact that the original model performs 220 epochs with 1000 steps each, the training time will be too much for a normal/semi-advanced machine.
In order to increase performance, we used the cloud computing service [FloydHub](https://www.floydhub.com) for our computations; this gave us access to the GPU memory usage and since we noticed that only about 50% of said memory was in use at any moment we increased the *GPU_PER_IMAGE* parameter to 4.

Moreover we decreased the *STEPS_PER_EPOCH* to 100 and the images min/max size to respectively 128/512 to further speed up the operations.


# Osservation
- Using coco + visiope if you set the *DETECTION_MAX_INSTANCES* parameter to 50 the mrcnn_class_loss goes to infinity

## Eval Comapring



|Model used | Eval score  | Not Predicted    | Centroid Fail   | Centroid Thr  | Area Thr  | Min confidence|
|:---------:|:-----------:|:----------------:|:---------------:|:-------------:|:---------:|:-------------:|
| 25        | 41.41       | 38.57            | 41.55           |  0.5          | 0.5       |  0.8          |
| 70        | 44.39       | 41.85            | 43.65           |  0.5          | 0.5       |  0.8          |
| 85        | 46.86       | 40.12            | 41.04           |  0.5          | 0.5       |  0.8          | 
| 25        | 46.49       | 38.57            | 36.47           |  1            | 0.5       | 0.8           |
| 70        | 48.43       | 41.85            | 39.61           |  1            | 0.5       | 0.8           |
| 85        | 49.62       | 41.04            | 34.46           |  1            | 0.5       | 0.8           |

|Model used | Eval score  | Not Predicted    | Wrong Predictions   | Centroid Thr  | Area Thr  | Min confidence|
|:---------:|:-----------:|:----------------:|:---------------:|:-------------:|:---------:|:-------------:|
| 25        | 43.52       | 31.3             | 25.19               |  0.5          | 0.5       | 0.6           |
| 70        | 46.79       | 36.62            | 16.59               |  0.5          | 0.5       | 0.6           |
| 85        | 48.09       | 36.95            | 14.99               |  0.5          | 0.5       | 0.6           |
| 25        | 47.24       | 31.3             | 21.46               |  0.7          | 0.5       | 0.6           |
| 70        | 50.22       | 36.62            | 13.15               |  0.7          | 0.5       | 0.6           |
| 85        | 50.99       | 36.95            | 12.06               |  0.7          | 0.5       | 0.6           |



|Model used | Precision | Recall  |   F1    | Centroid Thr  | Area Thr  | Min confidence|
|:---------:|:---------:|:-------:|:-------:|:-------------:|:---------:|:-------------:|
| 25        |  45.57    |  42.92  |  44.21  |      0.5      |    0.5    |      0.6      |
| 70        |  56.93    |  46.5   |  51.19  |      0.5      |    0.5    |      0.6      |
| 85        |  60.93    |  46.94  |  53.03  |      0.5      |    0.5    |      0.6      |




# TODO 

## Visiope
- [X] Train the model for decent detection
- [X] Implement evaluation 
- [] Fine tune parameters
- [] Large weight decay
- [X] use resnet 50
- [X] Fix bk issue
- [] gradient clipping
- [] real-time video
- [] concat output + attention + one-hot with all ones in positions of all objects appeared in video

## Activity Recognition

### Dataset generation

The output of the detection must be a dictionary with the following keys:
- activity: name of the musical instrument
- frames: list of elements, each element's index must be correlated to a frame.

These elements will be in a list, which will hold dictionaries.
Each dict will represent a detection and will hold the following data:
- id
- class 
- confidence 
- bounding box
- polygon

Example: we have a video of 100 frames so the output list len must be 100.
In frame 0 the masks *person* and *sax* are detected se we will have two dictionaries holding the above mentioned values.
These dicts will be put in a list which will be later appended to the list *frames*

### Output vector generation
For each frame the list of detected objects in that frame is analyzed. For each object instance the centroid and area are
computed and then the radius of the area (approximated to a circle) is computed and multiple checks are done. If the musical
instrument is detected near another class specific object (e.g. mouth and hand for saxophone) then that frame is 
considered positive, meaning that in that specific frame the activity of playing that instrument was detected.
Afterwards to avoid the frame-skipping phenomenon or changes of perspective a sliding window approach has been applied
in the following way. For each frame *N* frames are considered left and *N* right. If at least X% of the surrounding
frames detected the activity, then also the central frame is considered as if it detected the activity.

### Optimization

-[X] For videos with a high frame rate introduce a frame skipping algorithm, declaring a minimum FPS
-[X] Remove frames with no found objects

### Other
- [] Implement accuracy in tensorflow
- [] Use validation set
- [] Comment more
- [] Use mean value for maximum video per category

# Activity Recognition

## Inputs

### Building inputs
For time optimization reasons we decided to convert every video into a cPickle object. The procedure is the following:
- Read every frame from the video, skipping every *x* frames where *x* is estimated by bringing the video's FPS to 15 maximum.
- For every frame, detect the objects using the restored model
- For every object we now have: rois, mask, class_id and score
- Perform area and centroid estimation for every object and discard the mask which lead to a memory optimization by a factor of 1000x
- Save everything into a dictionary with the keys [frames (list of frames), activity : name of the activity]

This saved us a lot of time when building and debugging the LSTM model (weeks probably).

### Filtering inputs
Now we can just read a pickle object for every video to load the data. When doing so we perform other task such as:
- Removing every 'person' object in frames, this is due the fact that person is in every thus procucing a sort of white noise. 
Persors are like stopwords, so not ery informative for our goal
- Optionally removing every coco object
- Filtering out objects with low detection confidence (<0.75)
- Removing empty frames
- Counting number of empty frames and dividing by the total, if the reuslt is less than 10% discard the video
- Since there are some inconsistency in the video quantity for each category (minimum 10 , maximum  100) we decided to keep a mean value for every category

### Other
Before feeding the model with our inputs we perform other operation such as:
- Determine the mean quantity of frames for each video and its STD. Then randomly choose *mean+std*threshold* frames out of every video, 
this assures that no video will prevail on other which are shorter. The random choice guarantees to take useful
frames and features sampled across the entire video.
- Randomly shuffle both the video order and the frames in the video. The latter are then filtered out eith the previously cited algorithm and the sorted
back to their original order
- Data augmentation: each video is taken for training sampling *N* random frames from it. To enlarge
the training data this approach has been done *X* times for each video so that the dataset is enlarged
*X* times. Moreover the random sampling also guarantees that the selected frames are never the same.

### Finally 
Since the input processing (from reading the pickle files to having a list of frames for each video) can take up to several minutes, 
we decided to use the *ujson* library to dump the data after the processing and load it from the second run until the end.
This method requires just about two seconds to execute, thus saving us time to debug the model.

 
## Model
### Input
The input is a fixed-lenght padded sequence of frame representations of a video. Each frame is a one-hot vector
with a one in the position of the objects appearing in that frame.

###Structure
The model is based on two stacked Bi-LSTMs and was chosen to effectively model sequences of frames in a temporal way.
The model takes in input a fixed-length padded sequence of frame representations which is fed into the Bi-LSTM. This last
will output a sequence of hidden representation of the inputs. Each word has as hidden output the concatenation
of the forward and backward outputs. Since our model is "Many-to-one' we just keep the
hidden output of the last words in the sequence. Since the sequences are padded first the padding is removed and
then the last word's representation in selected. Afterwards the a single attention vector is computed 
for the entire sentence and in concatenated to the last hiden output of the Bi-LSTM. 
Then this concatenated vector is fed into a dense layer which outputs a logits vector. To this vector
a softmax function is applied in order to obtain a probability distribution among the possible output 
classes for the input video. 
A cross-entropy loss function is then defined between the this probability and the actual class of the video.
This error is then minimized through the Adam optimizer 

###Output
Given the probability distribution of the prediction of the class it is sufficient to take the argmax of this 
vector in order to obtain the predicted video class of the input video.

### Single attention mechanism
This mechanism was introduced in order to help the network to focus on the frames and features relevant 
for its purpose. The attention vector is computed as follows:
*WRITE THE NLP SLIDES FORMULA FOR ATTENTION*
This improved a lot the performances.

### Results
- The obtained scores are...
- Display the confusion matrix




# Links
- [coco matterport](https://github.com/matterport/Mask_RCNN)
- [imgaug](https://github.com/aleju/imgaug)
- [coco tools](https://github.com/waleedka/coco)
- [coco fork](https://github.com/pdollar/coco )
- [coco pull request](https://github.com/cocodataset/cocoapi/pull/50)
- [coco weights](https://github.com/matterport/Mask_RCNN/releases)
- [balloon blog post](https://engineering.matterport.com/splash-of-color-instance-segmentation-with-mask-r-cnn-and-tensorflow-7c761e238b46?gi=e0db3414c1ab)
