import json
import os

import numpy as np
import scipy
import skimage
from mrcnn import utils
from mrcnn.config import Config
from pycocotools import mask as maskUtils
from pycocotools.coco import COCO
import pycocotools

############################################################
#  Configurations
############################################################
from utils import compare_class_info


class CocoConfig(Config):
    """Configuration for training on a local machine with both coco and visiope dataset.
        Derives from the base Config class and overrides values specific
        to the DATADIR.
        """

    # Give the configuration a recognizable name
    NAME = "visiope"

    # We use a GPU with 12GB memory, which can fit two images.
    # 6GB per Image
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1

    # Uncomment to train on 8 GPUs (default is 1)
    # GPU_COUNT = 8

    # Number of classes (including background)
    NUM_CLASSES = 12 + 20 + 1

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 1500

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 50

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Minimum probability value to accept a detected instance
    # ROIs below this threshold are skipped
    DETECTION_MIN_CONFIDENCE = 0.8

    # Non-maximum suppression threshold for detection
    # DETECTION_NMS_THRESHOLD = 0.7

    # Max number of final detections
    #DETECTION_MAX_INSTANCES = 60 # do not set to 40 -> class loss goes to infinity

    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    #RPN_NMS_THRESHOLD = 0.2

    # Weight decay regularization
    #WEIGHT_DECAY = 0.000001

    # Learning rate and momentum
    # The Mask RCNN paper uses lr=0.02, but on TensorFlow it causes
    # weights to explode. Likely due to differences in optimzer
    # implementation.
    #LEARNING_RATE = 0.00001
    #LEARNING_MOMENTUM = 0.87

    # Backbone network architecture
    # Supported values are: resnet50, resnet101
    BACKBONE = "resnet101"


    # Anchor stride
    # If 1 then anchors are created for each cell in the backbone feature map.
    # If 2, then anchors are created for every other cell, and so on.
    RPN_ANCHOR_STRIDE = 1




class VisiopeConfig(Config):
    """Configuration for training on a local machine with visope dataset.
    Derives from the base Config class and overrides values specific
    to the DATADIR.
    """

    # Give the configuration a recognizable name
    NAME = "visiope"

    # We use a GPU with 12GB memory, which can fit two images.
    # 6GB per Image
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1

    # Uncomment to train on 8 GPUs (default is 1)
    # GPU_COUNT = 8

    # Number of classes (including background)
    NUM_CLASSES = 13

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 100

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 50

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Minimum probability value to accept a detected instance
    # ROIs below this threshold are skipped
    # DETECTION_MIN_CONFIDENCE = 0.8

    # Non-maximum suppression threshold for detection
    # DETECTION_NMS_THRESHOLD = 0.7

    # Max number of final detections
    # DETECTION_MAX_INSTANCES = 100

    # Weight decay regularization
    # WEIGHT_DECAY = 0.0007

    # LEARNING_RATE = 0.0001


class VisiopeConfigFloyd(Config):
    """Configuration for training on floyd (or any powerfull machine)
    Derives from the base Config class and overrides values specific
    to the DATADIR.
    """

    # Give the configuration a recognizable name
    NAME = "visiope"

    # We use a GPU with 12GB memory, which can fit two images.
    # 6GB per Image
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 2

    # Uncomment to train on 8 GPUs (default is 1)
    # GPU_COUNT = 8

    # Number of classes (including background)
    NUM_CLASSES = 13

    # Number of training steps per epoch
    STEPS_PER_EPOCH = 400

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 50

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 128
    IMAGE_MAX_DIM = 512

    # Minimum probability value to accept a detected instance
    # ROIs below this threshold are skipped
    # DETECTION_MIN_CONFIDENCE = 0.8

    # Non-maximum suppression threshold for detection
    # DETECTION_NMS_THRESHOLD = 0.7

    # Max number of final detections
    # DETECTION_MAX_INSTANCES = 100

    # Weight decay regularization
    # WEIGHT_DECAY = 0.0007

    # LEARNING_RATE = 0.0001


############################################################
#  Dataset
############################################################

class VisiopeDataset(utils.Dataset):
    """
        The overridden Coco class
        Attributes:
            json_path (str): the complete path to the json file
            path_bmp/pnh (str): the path to the two folders
            image_base_name (str): the base name for an image

            json_list (list): a list of dictionaries which are the json entities
            classes (dict): a dictionary which maps a class (str) with an int
            custom_images (list): a list of dictionaries which contains useful info about images
    """

    def __init__(self, visiope_data_dir, json_name, coco_data_dir, coco_flag=False):
        """
        Load the CocoDataset
        :param visiope_data_dir: the path to the dir in which there are the json and the png/bmp dirs
        :param json_name: the name of the json file
        """

        super().__init__()
        self.path = visiope_data_dir
        self.json_name = json_name
        self.json_path = os.path.join(self.path, self.json_name)
        self.path_bmp = os.path.join(self.path, "bmpImages/")
        self.path_png = os.path.join(self.path, "pngImages/")
        self.image_base_name = "image"
        self.max_coco_images = 102  # 42 if not working

        self.json_list = json.load(open(self.json_path))
        self.classes, self.custom_images = self.load_ids_classes(self.json_list, visiope_data_dir)

        self.coco_flag = coco_flag
        self.coco_data_dir = coco_data_dir

        if coco_flag:
            self.coco = COCO(f"{visiope_data_dir}/coco_instances.json")

    def load_and_prepare(self, coco_flag):
        """
        First loads visiope, then sorts its calsses
        (to use only when you are sure that the number of classes in the train dir is equal to the one in the aval dir),
        load coco's stuff too and prepare the whole dataset for the next phase
        :return:
        """

        self.load_visiope()

        class_info = compare_class_info([], self.class_info, coco_flag)
        self.set_class(class_info)

        if self.coco_flag:
            self.load_coco()

        self.prepare()

    def load_ids_classes(self, json_list, dataset_dir):
        """
        Generate the classes, custom images needed for mask generation
        :param json_list: a list of json entities
        :param dataset_dir: the path in which the pg/bmp images are located
        :return:
            classes: (list) a list of dict which maps a class (str) to an id (int)
            image_ids: (list) a list of dict with useful infos about an image (not mask)
        """
        classes = {}  # a list of all possible classes without duplicates
        image_ids = []  # riempire con gli id di tutte le immagini non skippate

        idx = 0
        idx_2 = 0
        # start the loop for every entity in the json list
        for json_elem in json_list:

            # skip the skipped images
            if json_elem['Label'] == 'Skip':
                continue

            # add class to class list
            for label in json_elem['Label'].keys():
                if label not in classes.keys():
                    classes[label] = len(classes) + 1

            # read image for sizes
            img_path = f"{dataset_dir}/pngImages/image{idx}.png"
            try:
                img = skimage.io.imread(img_path)
                height, width = img.shape[:2]
                # height, width=2,2

                # generate image dict
                img_dict = {
                    'id': idx_2,
                    'path': img_path,
                    'polys': json_elem['Label'],
                    'h': height,
                    'w': width
                }
                idx_2 += 1
                # add dict to list and increment idx
                image_ids.append(img_dict)
            except ValueError:

                print(img_path)
            # increment idx even if error occurs, sicne image labels are precisely like this
            idx += 1

        return classes, image_ids

    def set_class(self, class_info):

        self.class_info = class_info

        #fixme: instead of creating a list extend a distionary with the given name/id, then remove the generator call (look for y_prov) in the mask loading
        new_classes = {}
        for elem in class_info:
            new_classes[elem['name']]=elem['id']
            #new_classes.append({elem['name']: elem['id']})

        self.classes = new_classes

    def load_visiope(self):
        """
            Load the infos saved in this class (classes, custom_iamges) into the super class
            which is later used in the training/eval pahse
        """
        ################VISIOPE DATA###############
        # add classes
        for k, v in self.classes.items():
            self.add_class("visiope", v, k)

        # Add images
        for elem in self.custom_images:
            self.add_image(
                "visiope",  # source name
                image_id=elem['id'],  # image id (int) univoque
                path=elem['path'],
                polys=elem['polys'],  # a list of polygons inside the image
                h=elem['h'], w=elem['w'])  # sizes

    def load_coco(self, return_coco=False):

        ################COCO DATA###############

        class_ids0 = sorted(self.coco.getCatIds())


        list_to_keep = [
            'person', 'chair', 'tie', 'bottle', 'clock', 'cell phone',
            'keyboard', 'mouse', 'laptop', 'couch', 'tv', 'broccoli',
            'bus', 'train', 'truck', 'pizza', 'bed', 'remote', 'book', 'bench'
        ]

        class_ids = [id for id in class_ids0 if self.coco.loadCats(id)[0]["name"] in list_to_keep]

        image_ids = []
        for id in class_ids:
            image_ids.extend(list(self.coco.getImgIds(catIds=[id]))[:self.max_coco_images])
            # Remove duplicates
        image_ids = list(set(image_ids))
        # Add classes
        for i in class_ids:
            print(self.coco.loadCats(i)[0]["name"])
            self.add_class("coco", i, self.coco.loadCats(i)[0]["name"])

        # Add images
        for i in image_ids:
            self.add_image(
                "coco", image_id=i,
                path=os.path.join(self.coco_data_dir, self.coco.imgs[i]['file_name']),
                width=self.coco.imgs[i]["width"],
                height=self.coco.imgs[i]["height"],
                annotations=self.coco.loadAnns(self.coco.getAnnIds(
                    imgIds=[i], catIds=class_ids, iscrowd=None)))

        print(f"total number of classes is {len(self.class_info)}")

        if return_coco:
            return self.coco

    def load_mask(self, image_id):
        """Load instance masks for the given image.
        :returns:
            masks: A bool array of shape [height, width, instance count] with
                one mask per instance.
            class_ids: a 1D array of class IDs of the instance masks.
        """

        # empty mask
        empty_mask = super(VisiopeDataset, self).load_mask(image_id)

        # get the corresponging infos
        img_info = self.image_info[image_id]

        # skip if the image is not from visiope
        if img_info['source'] == 'visiope':
            return self.load_mask_visiope(img_info, empty_mask)

        elif img_info['source'] == "coco":
            return self.load_mask_coco(image_id, empty_mask)

        else:
            return empty_mask

    def load_mask_visiope(self, img_info, empty_mask):

        masks_matrix = []
        y = []
        try:
            # for every polygon in the label
            for k, v in img_info['polys'].items():

                # if the polygon has the 'hand' in its name there will be more than one mask for it
                if 'hand' in k.lower() or 'congas' in k.lower():

                    # there are usually multiple maks for the hands
                    for hand_idx in range(len(v)):
                        # open the mask image and append it to the mask matrix
                        img_path = f"{os.path.splitext(img_info['path'])[0]}{k}{hand_idx}.png"
                        # use flatten to cancel out the colors channel
                        img = scipy.misc.imread(img_path, flatten=True)
                        masks_matrix.append(img)

                        # append the class idx to the y vector
                        #y_prov=next(elem[k] for elem in self.classes if k in elem.keys())
                        y.append(self.classes[k])
                # else there is just one mask
                else:
                    img_path = f"{os.path.splitext(img_info['path'])[0]}{k}0.png"
                    img = scipy.misc.imread(img_path, flatten=True)
                    masks_matrix.append(img)

                    # append the class idx to the y vector
                    #y_prov = next(elem[k] for elem in self.classes if k in elem.keys())
                    y.append(self.classes[k])
        except Exception as e:
            print(f"Caught exception: {e}")
            return empty_mask

        class_ids = np.array(y, dtype=np.int32)
        try:
            # try to concat every mask on the last axis, it may fail if some
            # images still have the rgb channel (but shoulden't)
            mask = np.stack(masks_matrix, axis=2).astype(np.bool)
            # return the mask with the class ids
            return mask, class_ids

        except ValueError:
            # if error return an empty mask
            print("bad")
            print("::::::::::::::::::::::::::::")

            return empty_mask


    def load_mask_coco(self, image_id, empty_mask):
        """Load instance masks for the given image.

                Different datasets use different ways to store masks. This
                function converts the different mask format to one format
                in the form of a bitmap [height, width, instances].

                Returns:
                masks: A bool array of shape [height, width, instance count] with
                    one mask per instance.
                class_ids: a 1D array of class IDs of the instance masks.
                """
        # If not a COCO image, delegate to parent class.
        image_info = self.image_info[image_id]
        instance_masks = []
        class_ids = []
        annotations = self.image_info[image_id]["annotations"]
        # Build mask of shape [height, width, instance_count] and list
        # of class IDs that correspond to each channel of the mask.
        for annotation in annotations:
            class_id = self.map_source_class_id(
                "coco.{}".format(annotation['category_id']))
            if class_id:
                m = self.annToMask(annotation, image_info["height"],
                                   image_info["width"])
                # Some objects are so small that they're less than 1 pixel area
                # and end up rounded out. Skip those objects.
                if m.max() < 1:
                    continue
                # Is it a crowd? If so, use a negative class ID.
                if annotation['iscrowd']:
                    # Use negative class ID for crowds
                    class_id *= -1
                    # For crowd masks, annToMask() sometimes returns a mask
                    # smaller than the given dimensions. If so, resize it.
                    if m.shape[0] != image_info["height"] or m.shape[1] != image_info["width"]:
                        m = np.ones([image_info["height"], image_info["width"]], dtype=bool)
                instance_masks.append(m)
                class_ids.append(class_id)

        # Pack instance masks into an array
        if class_ids:
            mask = np.stack(instance_masks, axis=2).astype(np.bool)
            class_ids = np.array(class_ids, dtype=np.int32)
            return mask, class_ids
        else:
            # Call super class to return an empty mask
            return empty_mask

    def image_reference(self, image_id):
        """Return a link to the image in the COCO Website."""
        info = self.path + "pngImages/image" + (str(image_id))
        return info
        # if info["source"] == "visiope":
        #     return info
        # else:
        #     super(VisiopeDataset, self).image_reference(image_id)

    # The following two functions are from pycocotools with a few changes.

    def annToRLE(self, ann, height, width):
        """
        Convert annotation which can be polygons, uncompressed RLE to RLE.
        :return: binary mask (numpy 2D array)
        """
        segm = ann['segmentation']
        if isinstance(segm, list):
            # polygon -- a single object might consist of multiple parts
            # we merge all parts into one mask rle code
            rles = maskUtils.frPyObjects(segm, height, width)
            rle = maskUtils.merge(rles)
        elif isinstance(segm['counts'], list):
            # uncompressed RLE
            rle = maskUtils.frPyObjects(segm, height, width)
        else:
            # rle
            rle = ann['segmentation']
        return rle

    def annToMask(self, ann, height, width):
        """
        Convert annotation which can be polygons, uncompressed RLE, or RLE to binary mask.
        :return: binary mask (numpy 2D array)
        """
        rle = self.annToRLE(ann, height, width)
        m = maskUtils.decode(rle)
        return m
