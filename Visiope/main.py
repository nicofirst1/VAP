import argparse
import os

from mrcnn import model as modellib

from VisiopeClass import VisiopeConfig, VisiopeConfigFloyd, CocoConfig
from main_functions import train, detect, evaluate_model

# parsing command line arguments
from utils import play_soud

parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument("--mode", help="""There are three modes:
                                    train: which trains the model with the coco weights
                                    eval: evaluate the model accuracy
                                    detect: apply the model detection onto some images""",
                    type=str, default="train", choices=['train', 'eval', 'detect'])

parser.add_argument("--model_weights", help="""Either the path to the model .h5 weight, or 
                                            'last': to load the last model from the logdir
                                            'imagenet': to load the imagenet models""",
                    type=str, default="mask_rcnn_coco_v2.h5")

parser.add_argument("--floyd", help="If to activate the floyd mode", type=bool, default=False, choices=[True, False])
parser.add_argument("--coco", help="If to activate the coco mode", type=bool, default=False, choices=[True, False])

# get the arguments
args = parser.parse_args()

if not args.floyd:
    # Directory to save logs and model checkpoints, if not provided
    # through the command line argument --logs
    DEFAULT_LOGS_DIR = os.path.join(os.getcwd(), "Logs")

    # The data dir must be configured in the following way: a dir called DATADIR at the same level of Visiope,
    #  inside this dir there will be the '.h5' files and another two dirs called 'Train' and 'Eval'.
    #  in this dirs the wil be a json file called 'json_name.json' and the png/bmp folders
    json_name = "merged.json"
    DATA_DIR = os.path.join(os.getcwd(), "../DATADIR")
    visiope_data_train = os.path.join(DATA_DIR, "Visiope_data/Train")
    visiope_data_eval = os.path.join(DATA_DIR, "Visiope_data/Eval")
    coco_data_train = os.path.join(DATA_DIR, "Coco_data/Train")
    coco_data_eval = os.path.join(DATA_DIR, "Coco_data/Eval")

    # choose the right config file for the task
    if args.coco:
        config = CocoConfig()
        config.display()
    else:
        config = VisiopeConfig()
        config.display()

else:
    # Directory to save logs and model checkpoints, if not provided
    # through the command line argument --logs
    DEFAULT_LOGS_DIR = '/output'

    ### PATH VARIABLES
    # The data dir must be configured in the following way: a dir called DATADIR at the same level of Visiope,
    #  inside this dir there will be the '.h5' files and another two dirs called 'Train' and 'Eval'.
    #  in this dirs the wil be a json file called 'json_name.json' and the png/bmp folders
    json_name = "merged.json"
    DATA_DIR = '/DATADIR'
    visiope_data_train = os.path.join(DATA_DIR, "Visiope_data/Train")
    visiope_data_eval = os.path.join(DATA_DIR, "Visiope_data/Eval")
    coco_data_train = os.path.join(DATA_DIR, "Coco_data/Train")
    coco_data_eval = os.path.join(DATA_DIR, "Coco_data/Eval")

    # set cconfig and display them
    if args.coco:
        config = CocoConfig()
        config.display()
    else:
        config = VisiopeConfig()
        config.display()

#  model weights ca be either
#  the path to the .h5
#  'last': to load the last model from the logdir
#  'imagenet': to load the imagenet models
if "/" not in args.model_weights:
    args.model_weights = os.path.join(DATA_DIR, args.model_weights)

# the number of image to detect if the mode in 'detect'
detection_step = 10

EVAL_LIMIT = 5  # number of images to run the evaluation on

# epochs
ep1 = 35  # original 40
ep2 = ep1 + 35  # original 120
ep3 = ep2 + 15  # original 160

# Create model
if args.mode == "train":
    model = modellib.MaskRCNN(mode="training", config=config,
                              model_dir=DEFAULT_LOGS_DIR)
else:
    model = modellib.MaskRCNN(mode="inference", config=config,
                              model_dir=DEFAULT_LOGS_DIR)

if args.model_weights.lower() == "last":
    # Find last trained weights
    model_path = model.find_last()[1]
elif args.model_weights.lower() == "imagenet":
    # Start from ImageNet trained weights
    model_path = model.get_imagenet_weights()
else:
    model_path = args.model_weights

train_data = [visiope_data_train, coco_data_train]
eval_data = [visiope_data_eval, coco_data_eval]

if args.mode == "train":
    train(model, model_path, config, train_data, eval_data, json_name, ep1=ep1, ep2=ep2, ep3=ep3, coco_flag=args.coco)
    play_soud(0.5,440,3)
if args.mode == "eval":
    evaluate_model(model, eval_data, json_name, EVAL_LIMIT=EVAL_LIMIT)

elif args.mode == "detect":
    detect(model, model_path, eval_data, json_name, config, detection_step=detection_step, coco_flag=args.coco)

else:
    print("'{}' is not recognized. "
          "Use 'train' or 'evaluate'".format(args.mode))


