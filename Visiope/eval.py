import cv2
import random
import time

import mrcnn.model as modellib
import numpy as np
from mrcnn import visualize
from pycocotools import mask as maskUtils
from pycocotools.cocoeval import COCOeval


def build_coco_results(dataset, image_ids, rois, class_ids, scores, masks):
    """Arrange resutls to match COCO specs in http://cocodataset.org/#format
    """
    # If no results, return an empty list
    if rois is None:
        return []

    results = []
    for image_id in image_ids:
        # Loop through detections
        for i in range(rois.shape[0]):
            class_id = class_ids[i]
            score = scores[i]
            bbox = np.around(rois[i], 1)
            mask = masks[:, :, i]

            result = {
                "image_id": image_id,
                "category_id": dataset.get_source_class_id(class_id, "visiope"),
                "bbox": [bbox[1], bbox[0], bbox[3] - bbox[1], bbox[2] - bbox[0]],
                "score": score,
                "segmentation": maskUtils.encode(np.asfortranarray(mask))
            }
            results.append(result)
    return results


def evaluate_coco(model, dataset, coco, eval_type="segm", limit=0, image_ids=None):
    """Runs official COCO evaluation.
    DATADIR: A Dataset object with valiadtion data
    eval_type: "bbox" or "segm" for b100ounding box or segmentation evaluation
    limit: if not 0, it's the number of images to use for evaluation
    """
    # Pick COCO images from the DATADIR
    image_ids = image_ids or dataset.image_ids

    # Limit to a subset
    if limit:
        image_ids = image_ids[:limit]

    # Get corresponding COCO image IDs.
    coco_image_ids = [dataset.image_info[id]["id"] for id in image_ids]

    t_prediction = 0
    t_start = time.time()

    results = []
    for i, image_id in enumerate(image_ids):
        # Load image
        image = dataset.load_image(image_id)

        # Run detection
        t = time.time()
        r = model.detect([image], verbose=0)[0]
        t_prediction += (time.time() - t)

        # Convert results to COCO format
        # Cast masks to uint8 because COCO tools errors out on bool
        image_results = build_coco_results(dataset, coco_image_ids[i:i + 1],
                                           r["rois"], r["class_ids"],
                                           r["scores"],
                                           r["masks"].astype(np.uint8))
        results.extend(image_results)

    # Load results. This modifies results with additional attributes.
    coco_results = coco.loadRes(results)

    # Evaluate
    cocoEval = COCOeval(coco, coco_results, eval_type)
    cocoEval.params.imgIds = coco_image_ids
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    print("Prediction time: {}. Average {}/image".format(
        t_prediction, t_prediction / len(image_ids)))
    print("Total time: ", time.time() - t_start)


def detection(dataset, config, model):
    """
    Execute the detection pahse on a random image with a model
    :param dataset: the dataset from which to take the image
    :param config: the cnfig file
    :param model: the model to use for the detection
    :return: None
    """

    # get a random object from the visiope images subset
    visope_images = [elem for elem in dataset.image_info if elem['source'] == "visiope"]
    info = random.choice(visope_images)
    # get the index of the random object
    image_id = dataset.image_info.index(info)

    print("image ID: {}.{} ({}) {}".format(info["source"], info["id"], image_id,
                                           dataset.image_reference(image_id)))

    # read the various infos from the images
    image, _, _, _, _ = \
        modellib.load_image_gt(dataset, config, image_id, use_mini_mask=False)

    # Run object detection
    results = model.detect([image], verbose=1)

    # Display results
    r = results[0]
    visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                                dataset.class_names, r['scores'],
                                title="Predictions")

def detect_video(path2video, model):


    cap = cv2.VideoCapture(path2video)
    detections=[]


    # Check if camera opened successfully
    if not cap.isOpened():
        print("Error opening video stream or file")
        return


    while cap.isOpened():
        ret, frame = cap.read()

        if ret:
            results = model.detect([frame], verbose=1)
            detections.append(results[0])


    cap.release()

    return detections
