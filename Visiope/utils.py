import os

def compare_class_info(class_info1, class_info2, coco_flag):
    """
    Given to class_infos returns a merged one

    :param class_info1:
    :param class_info2:
    :return: new merged class  info
    """
    # get the names of each info
    c1_names = [elem['name'] for elem in class_info1 if elem['source'] == "visiope"]
    c2_names = [elem['name'] for elem in class_info2 if elem['source'] == "visiope"]

    # add them together and remove the duplicates
    diff = c1_names + c2_names
    diff = list(set(diff))
    diff.sort()

    # initialize new class info and add BG
    new_class_info=[]
    new_class_info = [{
        'source': 'visiope',
        'id': 0,
        'name': 'BG'
    }]

    # for every name add a new entry in the list
    for names in diff:
        if names != "BG":
            new_class_info.append({
                'source': 'visiope',
                'id': len(new_class_info),
                'name': names
            })

    return new_class_info


def remove_hand_class(json):
    for elem in json:

        if isinstance(elem['Label'], dict):
            for label in elem['Label']:
                if label == "hand" or label == "hands":
                    print(elem)


def play_soud(duration=1,freq=500, repet=1):
    """
    Play a sound
    :param duration: duration in seconds
    :param freq: frequence of the sound
    :return:
    """

    for i in range(repet):
        os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))
