import os

import keras
import numpy as np
from imgaug import augmenters as iaa
from mrcnn import visualize

from VisiopeClass import VisiopeDataset
from eval import detection, evaluate_coco, detect_video
from utils import compare_class_info

visual_check = False


def train(model, model_path, config, train_data, eval_data, json_name, ep1=40, ep2=120, ep3=160, coco_flag=False):
    """
    Execute the training with given parameters
    :param model: (class modellib.MaskRCNN), the model
    :param model_path: (str) the path to the model weights
    :param config: (class mrcnn.config.Config) the configurations for the model
    :param train_data: (str) path to train dataset
    :param eval_data: (str) path to evaluation dataset
    :param json_name: (str) the basename of the json
    :param ep1: (int) Optional, the number of epochs for the head training
    :param ep2: (int) Optional, the number of epochs for the first 4+ layer of the training
    :param ep3: (int) Optional, the number of epochs for the full training
    :return: None
    """

    # Exclude the last layers because they require a matching
    # number of classes
    model.load_weights(model_path, by_name=True, exclude=[
        "mrcnn_class_logits", "mrcnn_bbox_fc",
        "mrcnn_bbox", "mrcnn_mask"])

    # Training DATADIR. Use the training set and 35K from the
    # validation set, as as in the Mask RCNN paper.
    print("Loading train data...")
    dataset_train = VisiopeDataset(train_data[0], json_name, train_data[1], coco_flag=coco_flag)
    dataset_val = VisiopeDataset(eval_data[0], json_name, eval_data[1], coco_flag=coco_flag)

    dataset_train.load_visiope()
    dataset_val.load_visiope()

    # assign the right class ids
    class_info = compare_class_info(dataset_train.class_info, dataset_val.class_info, coco_flag)
    dataset_val.set_class(class_info)
    dataset_train.set_class(class_info)

    if coco_flag:
        dataset_train.load_coco()
        dataset_val.load_coco()

    # prepare the dataset after the right class idx assignment
    dataset_train.prepare()
    dataset_val.prepare()

    if visual_check:
        image_ids = np.random.choice(dataset_train.image_ids, 4)
        for image_id in image_ids:
            image = dataset_train.load_image(image_id)
            mask, class_ids = dataset_train.load_mask(image_id)
            visualize.display_top_masks(image, mask, class_ids, dataset_train.class_names)

        image_ids = np.random.choice(dataset_val.image_ids, 4)
        for image_id in image_ids:
            image = dataset_val.load_image(image_id)
            mask, class_ids = dataset_val.load_mask(image_id)
            visualize.display_top_masks(image, mask, class_ids, dataset_val.class_names)

    # Image Augmentation
    # Right/Left flip 50% of the time
    print("Augmenting data...")

    # # Image augmentation
    # # http://imgaug.readthedocs.io/en/latest/source/augmenters.html

    augmentation = iaa.Sometimes(1.0,
        iaa.Fliplr(1.0),
        iaa.Affine(rotate=(-45, 45))
    )



    # augmentation = iaa.Fliplr(0.5)

    # custom callbacks for early stopping
    callbacks1 = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.05, patience=ep1 / 2, verbose=0, mode='min'),

        keras.callbacks.EarlyStopping(monitor='val_mrcnn_mask_loss', min_delta=0.05, patience=ep1 / 2, verbose=0,
                                      mode='min'),
    ]

    callbacks2 = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.05, patience=ep2 / 2, verbose=0, mode='min'),

        keras.callbacks.EarlyStopping(monitor='val_mrcnn_mask_loss', min_delta=0.05, patience=ep2 / 2, verbose=0,
                                      mode='min'),
    ]

    callbacks3 = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.05, patience=ep3 / 2, verbose=0, mode='min'),

        keras.callbacks.EarlyStopping(monitor='val_mrcnn_mask_loss', min_delta=0.05, patience=ep3 / 2, verbose=0,
                                      mode='min'),
    ]

    no_augmentation = ['coco']
    # *** This training schedule is an example. Update to your needs ***

    if ep1 > 0:
        # Training - Stage 1
        print("Training network heads")
        model.train(dataset_train, dataset_val,
                    learning_rate=config.LEARNING_RATE,
                    epochs=ep1,
                    layers='heads',
                    augmentation=augmentation,
                    custom_callbacks=callbacks1,
                    no_augmentation_sources=no_augmentation)

    if ep2 > 0:
        # Training - Stage 2
        # Finetune layers from ResNet stage 4 and up
        print("Fine tune Resnet stage 4 and up")
        model.train(dataset_train, dataset_val,
                    learning_rate=config.LEARNING_RATE,
                    epochs=ep2,
                    layers='4+',
                    augmentation=augmentation,
                    custom_callbacks=callbacks2,
                    no_augmentation_sources=no_augmentation)
    if ep3 > 0:
        # Training - Stage 3
        # Fine tune all layers
        print("Fine tune all layers")
        model.train(dataset_train, dataset_val,
                    learning_rate=config.LEARNING_RATE / 10,
                    epochs=ep3,
                    layers='all',
                    augmentation=augmentation,
                    custom_callbacks=callbacks3,
                    no_augmentation_sources=no_augmentation)


def detect(model, model_path, eval_data, json_name, config, detection_step=5, coco_flag=False):
    """
    Execute the detection with the specific model on 'detection step' images
    :param model: (class modellib.MaskRCNN), the model
    :param model_path: (str) the path to the model weights
    :param EVAL_DIR: (str) path to evaluation dataset
    :param config: (class mrcnn.config.Config) the configurations for the model
    :param json_name: (str) the basename of the json
    :param detection_step: (int) Optional, number of images to perform the detection onto
    :return:
    """

    # Validation DATADIR
    print("Loading validation data...")
    dataset_val = VisiopeDataset(eval_data[0], json_name, eval_data[1], coco_flag=coco_flag)

    dataset_val.load_and_prepare(coco_flag)

    if visual_check:
        image_ids = np.random.choice(dataset_val.image_ids, 4)
        for image_id in image_ids:
            image = dataset_val.load_image(image_id)
            mask, class_ids = dataset_val.load_mask(image_id)
            visualize.display_top_masks(image, mask, class_ids, dataset_val.class_names)

    # Exclude the last layers because they require a matching
    # number of classes
    model.load_weights(model_path, by_name=True)

    for i in range(detection_step):
        detection(dataset_val, config, model)

def generate_dataset(model, model_path, eval_data, json_name):
    coco_flag=True
    videos_dir="/media/dizzi/data/DATASETs/Playing_musical_instruments"


    print("Loading validation data...")
    dataset_val = VisiopeDataset(eval_data[0], json_name, eval_data[1], coco_flag=coco_flag)

    dataset_val.load_and_prepare(coco_flag)


    model.load_weights(model_path, by_name=True)

    for class_dir in os.listdir(videos_dir):

        look_in=f"{videos_dir}/{class_dir}"

        for video in os.listdir(look_in):
            video_path = f"{look_in}/{video}"

            detect_video(video_path, model)



def evaluate_model(model, EVAL_DIR, json_name, EVAL_LIMIT=100):
    """
    Perform an evaluation on a model
    :param model: (class modellib.MaskRCNN), the model
    :param EVAL_DIR: (str) path to evaluation dataset
    :param json_name: (str) the basename of the json
    :param EVAL_LIMIT: (int) Optional, the number of images to perform evaluation onto
    :return:
    """
    # Validation DATADIR
    dataset_val = VisiopeDataset(EVAL_DIR[0], json_name, EVAL_DIR[1])
    coco = dataset_val.load_visiope(return_coco=True)
    dataset_val.prepare()
    print("Running COCO evaluation on {} images.".format(EVAL_LIMIT))
    evaluate_coco(model, dataset_val, coco, "segm", limit=int(EVAL_LIMIT))
