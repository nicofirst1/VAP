from JSONextractor import *
import os

curr_path = os.getcwd()
path = curr_path + '/../DATADIR/Eval/'
jsonName = "merged.json"

print(path)

paths = [path + "/" + jsonName,
         path,
         path + "/" + "pngImages",
         path + "/" + "bmpImages"]


test = JSONextractor(paths)
test.extraction()
test.testing()
duration = 3  # second
freq = 440  # Hz
os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))
