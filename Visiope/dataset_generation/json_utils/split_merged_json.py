import json
from random import shuffle
import os

curr_path = os.getcwd()
path = curr_path + '/../../DATADIR/Jsons/'
train_path = curr_path + '/../../DATADIR/Train/'
eval_path = curr_path + '/../../DATADIR/Eval/'
merged_name = path + 'merged.json'

b = json.load(open(merged_name))
shuffle(b)

tot = len(b)

train = b[0:int(0.8*tot)]
test = b[int(0.8*tot):]

with open(train_path + 'merged.json', 'w') as outfile:
    json.dump(train, outfile)

with open(eval_path + 'merged.json', 'w') as outfile:
    json.dump(test, outfile)
