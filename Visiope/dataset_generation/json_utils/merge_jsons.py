import os

curr_path = os.getcwd()
path = curr_path + '/../../DATADIR/Jsons/'
new_file = path + 'merged.json'


first = True

if os.path.exists(new_file):
    os.remove(new_file)

with open(new_file, 'a') as append_file:
    append_file.write('[')

file_names = os.listdir(path)
file_names.sort()

for filename in file_names:
    if filename.endswith('.json') and not filename.startswith('merged'):
        with open(path + filename, 'r') as read_file:
            content = read_file.read().split('[', 1)[1].rsplit(']', 1)[0]
            with open(new_file, 'a') as append_file:
                if first:
                    first = False
                else:
                    append_file.write(',')
                append_file.write(content)

with open(new_file, 'a') as append_file:
    append_file.write(']')
