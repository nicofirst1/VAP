import json
import os

curr_path = os.getcwd()
path = curr_path + '/../../DATADIR/Jsons/'
merged_path = path + 'merged.json'

json_obj = json.load(open(merged_path))
for ID in json_obj:
    for key in ID:
        if 'Mask' in key or 'Label' in key:
            for mask_key in ID[key]:
                if 'hand' in mask_key.lower():
                    ID[key]['hand'] = ID[key].pop(mask_key)
                if 'mouth' in mask_key.lower():
                    ID[key]['mouth'] = ID[key].pop(mask_key)

with open(merged_path, 'w') as outfile:
    json.dump(json_obj, outfile)
