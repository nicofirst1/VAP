import json
import urllib.request
import os
import time,re
from shapely.geometry import Polygon
from os import listdir
from os.path import isfile, join
from PIL import Image
from PIL import ImageFile, ImageDraw
ImageFile.LOAD_TRUNCATED_IMAGES = True

class JSONextractor():

    def __init__(self, paths):

        self.path = paths[0]
        self.directoryPath = paths[1]
        self.pngPath = paths[2]
        self.bmpPath = paths[3]
        self.keyDict = {}
        self.numberOfLabels = 0
        self.classes =[]
        self.numObject = []

        self.nomeBase = "image"
        self.labels = []

        os.chdir(self.directoryPath)

        if not os.path.exists(self.pngPath):
            os.makedirs(self.pngPath)

        if not os.path.exists(self.bmpPath):
            os.makedirs(self.bmpPath)

        b = json.load(open(self.path))
        for xx in range(len(b)):
            name = ''
            if b[xx]['Label'] == "Skip":
                continue
            for x in b[xx]['Label'].keys():
                name = x
                if name not in self.classes:
                    self.classes.append(name)
                    self.numObject.append(1)
                    self.labels.append(0)
                else:
                    self.numObject[self.classes.index(name)] += 1
        self.initStampa(self.classes, self.numObject)

    def initStampa(self, ogg, num):
        print("there are ", len(ogg), " objects.")
        count = 0
        for x in range(len(ogg)):
            print (ogg[x], " appears ", num[x], " times.")
            count += num[x]

        print ("There are ", count, " objects labeled in total.")


    def check_last_load(self):

        try:
            onlyfiles = [f for f in listdir(self.pngPath) if isfile(join(self.pngPath, f))]
            file_str="\n".join(onlyfiles)
            regex=re.compile(r"image([\d]+)")
            find=re.findall(regex,file_str)
            find=[int(elem) for elem in find]
            return max(find)
        except Exception:
            return 0


    def extraction(self):

        b = json.load(open(self.path))
        self.numberOfLabels = len(b)
        name = ''
        tot = len(b)
        start = self.check_last_load()

        count_skipped = 0
        if os.path.exists(self.directoryPath + '/' + 'skipped_counts.txt'):
            with open(self.directoryPath + '/' + 'skipped_counts.txt', 'r') as count_file:
                number = count_file.readline().strip()
                if len(number) > 0:
                    count_skipped = int(number)

        print(f'Restarting from image number {start + count_skipped}')
        idx_name_count = start

        for immNum in range(start  + count_skipped,len(b)):
            print('\rProgress: %d/%d - %4.2f%%' % (immNum + 1, tot, ((immNum + 1)/tot)*100), end="")
            if b[immNum]['Label'] == "Skip":
                count_skipped += 1
                with open(self.directoryPath + '/' + 'skipped_counts.txt', 'w') as count_file:
                    count_file.write(str(count_skipped))
                continue
            else:
                idx_name = idx_name_count
                idx_name_count += 1
            name = self.nomeBase + str(idx_name)
            imm = b[immNum]['Labeled Data']
            os.chdir(self.pngPath)

            idx = 5
            while idx > 0:
                try:
                    urllib.request.urlretrieve(imm, name + ".png")
                    break
                except Exception as e:
                    print(e)
                    time.sleep(1)
                    idx -= 1

            self.converti(name)

            if "Mask" in b[immNum].keys():
                print("single mask")
                for x in b[immNum]['Label'].keys():
                    name = x
                nameApp = name
                name = self.nomeBase + str(idx_name) + name
                imm = b[immNum]['Mask'][nameApp]
                os.chdir(self.pngPath)

                idx = 5
                while idx > 0:
                    try:
                        urllib.request.urlretrieve(imm, name + ".png")
                        break
                    except Exception as e:
                        print(e)
                        time.sleep(1)
                        idx -= 1

                os.chdir(self.bmpPath)

                self.converti(name)
            else:
                for x in range(len(self.labels)):
                    self.labels[x] = 0

                if len(b[immNum]['Label']) == 1:
                    for x in b[immNum]['Label'].keys():
                        name = x
                    nameApp = name
                    name = self.nomeBase + str(idx_name) + name
                    imm = b[immNum]['Masks'][nameApp]
                    os.chdir(self.pngPath)

                    idx = 5
                    while idx > 0:
                        try:
                            urllib.request.urlretrieve(imm, name + ".png")
                            os.rename(name + ".png", name + '0' + ".png")
                            break
                        except Exception as e:
                            print(e)
                            time.sleep(1)
                            idx -= 1

                    os.chdir(self.bmpPath)
                    self.converti(name + '0')
                else:
                    for x in b[immNum]['Label'].keys():
                        name = x
                        name = self.nomeBase + str(idx_name) + name + str(self.labels[self.classes.index(name)])
                        self.labels[self.classes.index(x)] += 1
                        imm = b[immNum]['Masks'][x]
                        os.chdir(self.pngPath)

                        idx = 2
                        retrieved = False
                        while idx > 0:
                            try:
                                urllib.request.urlretrieve(imm, name + ".png")
                                retrieved = True
                                break
                            except Exception as e:
                                print('HTTP REQUEST FAILED')
                                time.sleep(1)
                                idx -= 1
                        if not retrieved:
                            idx_name_count -= 1
                            count_skipped += 1
                            with open(self.directoryPath + '/' + 'skipped_counts.txt', 'w') as count_file:
                                count_file.write(str(count_skipped))
                            continue

                        os.chdir(self.bmpPath)

                        if 'hand' in x.lower() or 'conga' in x.lower():
                            hand_list = b[immNum]['Label'][x]
                            if len(hand_list) >= 2:
                                points_lists = []
                                for ii in range(len(hand_list)):
                                    points_dicts = hand_list[ii]
                                    points = []
                                    for pair_dict in points_dicts:
                                        xx = pair_dict['x']
                                        yy = pair_dict['y']
                                        points.append((xx,yy))
                                    points_lists.append(points)

                                self.converti_hands(name, points_lists, b, immNum, x)
                            else:
                                self.converti(name)
                        else:
                            self.converti(name)
            
        with open(self.path, 'w') as outfile:
            json.dump(b, outfile)


    def stampa(self):
        print(self.keyDict)

    def converti(self, name):
        path = self.pngPath + "/" + name + ".png"
        img = Image.open(path)
        if img.mode is not "RGB":
            img = img.convert("RGB")

        file_out = self.bmpPath + "/" + name + ".bmp"
        img.save(file_out)


    def converti_hands(self, name, points_lists, b, immNum, labelName):
        """
        :param name (str): image name
        :param points_lists (list): list of polygons
        """
        path = self.pngPath + "/" + name + ".png"
        img = Image.open(path)

        # Convert all images to RBG format
        if img.mode is not "RGB":
            img = img.convert("RGB")

        w, h = img.size

        # Compute the max area of the polygons
        max_area = 0
        for i in range(len(points_lists)):
            points = points_lists[i]
            poly_area = Polygon(points).area
            if poly_area > max_area:
                max_area = poly_area

        name_idx = 0
        for i in range(len(points_lists)):
            points = points_lists[i]
            poly_area = Polygon(points).area

            # If the area of the ccurrent poly in smaller than the max area / 5 then skip this poly
            if poly_area < (max_area / 5):
                b[immNum]['Label'][labelName] = b[immNum]['Label'][labelName][:i] + b[immNum]['Label'][labelName][i+1:]
                continue

            png_path = self.pngPath + "/" + name[:-1] + str(name_idx) + ".png"
            bmp_path = self.bmpPath + "/" + name[:-1] + str(name_idx) + ".bmp"

            # Draw the polygon
            poly = Image.new('RGB', (w, h), color=0)
            draw = ImageDraw.Draw(poly)
            draw.polygon(points, fill=(255,255,255,255), outline=(255,255,255,255))
            poly = poly.rotate(180).transpose(Image.FLIP_LEFT_RIGHT)

            # Save the polygon image in bmp and png formats
            poly.save(png_path)
            poly.save(bmp_path)

            name_idx += 1

    def testing(self):
        jsonPath = self.path
        b = json.load(open(jsonPath))
        classes = []
        image_ids = []  # riempire con gli id di tutte le immagini non skippate
        for xx in range(len(b)):
            if b[xx]['Label'] == "Skip":
                continue
            else:
                image_ids.append(xx)
            for x in b[xx]['Label'].keys():
                name = x
                if name not in classes:
                    classes.append(name)
        print("classes--> ", classes)
        print("images_ids --> ", image_ids)

