from icrawler.builtin import GoogleImageCrawler
import os

image_list=['playing guitarra']


def crawl_image(image_list,img_dir,images_per_item=50):

    for image in image_list:
        path=img_dir+f"/{image}/"
        os.makedirs(path)
        GoogleImageCrawler(storage={'root_dir': path}, downloader_threads=10).crawl(keyword=image, max_num=images_per_item)


crawl_image(image_list,'images',images_per_item=120)



